﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .table-bordered th {
            vertical-align: middle;
        }

            .table-bordered th a {
                color: #ffffff;
            }

        tr > th {
            background-color: #36b9cc !important;
        }

        .gridfloatright {
            text-align: right;
        }

        #ContentPlaceHolder1_GridCampaign tr > td {
            text-align: center !important;
            /* white-space: nowrap;
            line-height: 1 !important;*/
        }

        #ContentPlaceHolder1_grdinvoiceList tr > td {
            text-align: center !important;
            vertical-align: middle;
            /* white-space: nowrap;
            line-height: 1 !important;*/
        }

        /* .form-control {
            border-radius: 5px !important;
            height: 35px !important;
        }*/

        .displayNone {
            display: none;
        }

        .btn-save {
            text-transform: none !important;
            letter-spacing: 0 !important;
            min-width: 0px !important;
            padding: 0.375rem 0.75rem !important;
            border-radius: 21px !important;
        }

        .pull-right {
            float: right !important;
        }

        .input-group-addon {
            background-color: #EEEEEE;
            border: 1px solid #CCCCCC;
            border-radius: 4px 0px 0px 4px;
            font-weight: normal;
            line-height: 1;
            padding: 5px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border-right: none;
            margin: 0 0 0 -5px;
            display: table-cell;
        }

        .nav-link.active {
            background: red !important;
            color: white !important;
            font-weight: bold !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800 text-center" style="width: 100%" runat="server" id="lblAgencyName"></h1>
    </div>--%>
    <div class="card shadow mb-4">

        <div class="card tab-box">
            <div class="row user-tabs">
                <div class="col-lg-12 col-md-12 col-sm-12 line-tabs">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="nav-item"><a href="#TbMediaLog" data-toggle="tab" class="nav-link TbMediaLog active" style="padding: .50rem 0.75rem !important;">Media Log</a></li>
                        <li class="nav-item"><a href="#TbInvoiceList" data-toggle="tab" class="nav-link TbInvoiceList" style="padding: .50rem 0.75rem !important;">Invoice List</a></li>
                    </ul>
                </div>
            </div>
            <div class="tab-content" style="padding-top: 0px !important">
                <div class="tab-pane active" id="TbMediaLog" style="padding: 15px;">

                    <div class="card-header py-2">
                        <div class="row form-group">
                            <div class="col-md-2">
                                <label>
                                    From Date
                                </label>
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control" Style="height: 35px;"></asp:TextBox>
                                <asp:CalendarExtender ID="CCtxtNxtShipD" runat="server" DefaultView="Days" Format="MM/dd/yyyy"
                                    TargetControlID="txtFromDate">
                                </asp:CalendarExtender>
                            </div>
                            <div class="col-md-2">
                                <label>
                                    To Date
                                </label>
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Style="height: 35px;"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" DefaultView="Days" Format="MM/dd/yyyy"
                                    TargetControlID="txtToDate">
                                </asp:CalendarExtender>

                            </div>
                            <div class="col-md-3">
                                <br />
                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-danger btn-sm"
                                    Text="Search" Style="border-radius: 23px; height: 35px;" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                    </div>



                    <div class="card-body">
                        <div class="table-responsive">
                            <asp:GridView ID="GridCampaign" runat="server" AutoGenerateColumns="False" class="table table-bordered" EmptyDataText="No Record Found" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-Font-Bold="true">
                                <Columns>
                                    <asp:BoundField DataField="AutoId" HeaderText="AutoId" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                    <asp:BoundField DataField="AgencyDocumentPath" HeaderText="AgencyDocumentPath" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <center>
                                                <asp:ImageButton ID="ImgEdit" runat="server" Text="Add Document" CommandArgument='<%# Bind("AutoId") %>'
                                                    ToolTip="Add Document" ImageUrl="~/ScriptsAndCSS/CSS/Images/Uploadimage3.png" Style="height: 54px; width: 35px; border: 0px;" OnClick="ImgEdit_Click">
                                                </asp:ImageButton>
                                                &nbsp;
                                <asp:ImageButton ID="ImgMediaUpdate" runat="server" Text="Edit Media" CommandArgument='<%# Bind("AutoId") %>'
                                    OnClick="ImgMediaUpdate_Click" ToolTip="Edit Media" ImageUrl="~/ScriptsAndCSS/CSS/Images/edit2.png" Style="height: 54px; width: 35px; border: 0px;">
                                </asp:ImageButton>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View Document" ItemStyle-Width="30%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkImageFileName" runat="server" OnClick="lnkImageFileName_Click"
                                                Text='<% #Eval("AgencyDocumentName")%>'>LinkButton</asp:LinkButton>
                                            <br />
                                            <asp:LinkButton ID="lnkAgencyURL" runat="server" OnClick="lnkAgencyURL_Click"
                                                Text='<% #Eval("AgencyURL")%>'>LinkButton</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="LogDate" HeaderText="LogDate" />
                                    <asp:BoundField DataField="AgencyName" HeaderText="AgencyName" />
                                    <%-- <asp:BoundField DataField="AgencyDiscount" HeaderText="Agency Discount(%)" />--%>
                                    <asp:BoundField DataField="Station" HeaderText="Network" />
                                    <asp:BoundField DataField="SpotType" HeaderText="SpotType" />
                                    <%-- <asp:BoundField DataField="ProjectedTime" HeaderText="ProjectedTime" />--%>
                                    <%-- <asp:BoundField DataField="Spend" HeaderText="Gross Spend($)" ItemStyle-CssClass="Amt" />
                        <asp:BoundField DataField="TotalSpend" HeaderText="Net Spend($)" ItemStyle-CssClass="Amt" />--%>
                                    <%-- <asp:BoundField DataField="Creative" HeaderText="Creative" />--%>
                                    <asp:BoundField DataField="URL" HeaderText="URL" />
                                    <%-- <asp:BoundField DataField="InitialResponsePhone" HeaderText="Initial Response Phone" />
                        <asp:BoundField DataField="SpotLength" HeaderText="Spot Length" />--%>
                                    <asp:BoundField DataField="Type" HeaderText="Clearance" />
                                    <asp:BoundField DataField="Personality" HeaderText="Personality" />
                                    <%-- <asp:BoundField DataField="SourceKey" HeaderText="SourceKey" />--%>
                                    <%-- <asp:BoundField DataField="No800" HeaderText="DNIS" />--%>
                                    <%--<asp:BoundField DataField="HearAboutUs" HeaderText="Hear About Us" />
                        <asp:BoundField DataField="Promo" HeaderText="Promo Code" />--%>
                                    <%--<asp:BoundField DataField="ConversionRate" HeaderText="Conversion Rate" />
                        <asp:BoundField DataField="PhoneOrderTotal" HeaderText="Phone Order Total" />
                        <asp:BoundField DataField="PhoneOrderRevenue" HeaderText="Phone Order Revenue" />
                        <asp:BoundField DataField="WebOrderTotal" HeaderText="Web Order Total" />
                        <asp:BoundField DataField="WebOrderRevenue" HeaderText="Web Order Revenue" />--%>
                                    <%-- <asp:BoundField DataField="ActualTimeAired" HeaderText="ActualTimeAired" />--%>
                                </Columns>
                                <HeaderStyle CssClass="text-white bg-info text-center align-middle" />
                            </asp:GridView>

                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="TbInvoiceList" style="padding: 15px;">
                    <div class="card-header py-2">
                        <div class="row">
                            <div class="col-md-2" runat="server" visible="false">
                                <label>
                                    From Date
                                </label>
                                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Style="height: 35px;"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" DefaultView="Days" Format="MM/dd/yyyy"
                                    TargetControlID="txtFromDate">
                                </asp:CalendarExtender>
                            </div>
                            <div class="col-md-2" runat="server" visible="false">
                                <label>
                                    To Date
                                </label>
                                <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Style="height: 35px;"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" DefaultView="Days" Format="MM/dd/yyyy"
                                    TargetControlID="txtToDate">
                                </asp:CalendarExtender>

                            </div>
                            <div class="col-md-3" runat="server" visible="false">
                                <br />
                                <asp:Button ID="Button1" runat="server" CssClass="btn btn-danger btn-sm"
                                    Text="Search" Style="border-radius: 23px; height: 35px;" OnClick="btnSearch_Click" />
                            </div>
                            <div class="col-md-12" style="text-align: right;">
                                <asp:Button ID="btnAddInvoice" runat="server" CssClass="btn btn-danger btn-sm"
                                    Text="Add Invoice" Style="border-radius: 23px; height: 30px;" OnClick="btnAddInvoice_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <asp:GridView ID="grdinvoiceList" runat="server" AutoGenerateColumns="False" class="table table-bordered" EmptyDataText="No Record Found" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-Font-Bold="true">
                                <Columns>
                                    <asp:BoundField DataField="AutoId" HeaderText="AutoId" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                    <asp:BoundField DataField="InvoiceUploadDocumentPath" HeaderText="InvoiceUploadDocumentPath" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                    <asp:BoundField DataField="PaidReceiptDocumentPath" HeaderText="PaidReceiptDocumentPath" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                    <asp:TemplateField HeaderText="Invoice" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkInvoiceDocument" runat="server" OnClick="lnkInvoiceDocument_Click"
                                                Text='<% #Eval("InvoiceUploadDocumentName")%>'>LinkButton</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid Receipt" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPaidreceipt" runat="server" OnClick="lnkPaidreceipt_Click"
                                                Text='<% #Eval("PaidReceiptDocumentName")%>'>LinkButton</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Networkname" HeaderText="Network Name" />
                                    <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No" />
                                    <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date" />
                                    <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount($)" ItemStyle-CssClass="gridfloatright" />
                                     <asp:BoundField DataField="Terms" HeaderText="Terms" />
                                    <asp:BoundField DataField="Remark" HeaderText="Notes" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:BoundField DataField="InvoiceCreateddate" HeaderText="Created Date" />
                                    <asp:BoundField DataField="ActionRemark" HeaderText="Action Notes" />
                                    <asp:BoundField DataField="ActionBy" HeaderText="Action By" />
                                    <asp:BoundField DataField="ActionDate" HeaderText="Action Date" />
                                </Columns>
                                <HeaderStyle CssClass="text-white bg-info text-center align-middle" />
                            </asp:GridView>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="MPECancelOrder" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Upload Document</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Document Upload
                                </label>
                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="ReadOnlyText" Style="border-radius: 25px;" />
                                <asp:HiddenField runat="server" ID="hdnMediaCampaignAutoId" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Remark
                                </label>
                                <asp:TextBox ID="txtNotes" runat="server" CssClass="form-control"
                                    TextMode="MultiLine" Style="height: 50px !important; min-height: 0px !important" Rows="3" TabIndex="53"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    URL Upload
                                </label>
                                <asp:TextBox ID="txtURLUpload" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <br />
                            <br />
                            <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-success btn-sm pull-right btn-save"
                                Text="Save" OnClick="BtnSave_Click" />
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <%--  <div class="submit-section">
                        <asp:Button ID="Button5" runat="server" CssClass="btn btn-primary btn-sm"
                            Text="Revert Company Credit" OnClick="btnRevertCompanyCredit_Click" ValidationGroup="RevertCompanyCredit" />
                        <asp:Button ID="btnRevertCompanyCreditclosed" runat="server" Text="Close"
                            CssClass="btn btn-primary btn-sm" />
                    </div>--%>
                </div>
            </div>
        </div>
    </div>

    <div id="MPEUpdateMedia" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Edit Media</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Log Date
                                </label>
                                <asp:TextBox ID="txtLogDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" DefaultView="Days" Format="MM/dd/yyyy"
                                    TargetControlID="txtLogDate">
                                </asp:CalendarExtender>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Clearance
                                </label>
                                <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control">
                                    <asp:ListItem>-Select-</asp:ListItem>
                                    <asp:ListItem>Run</asp:ListItem>
                                    <asp:ListItem>Did Not Run</asp:ListItem>
                                    <asp:ListItem>Free Bonus Spot</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnUpdateMedia" runat="server" CssClass="btn btn-success btn-sm pull-right btn-save"
                                Text="Update" OnClick="btnUpdateMedia_Click" />
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <%--  <div class="submit-section">
                        <asp:Button ID="Button5" runat="server" CssClass="btn btn-primary btn-sm"
                            Text="Revert Company Credit" OnClick="btnRevertCompanyCredit_Click" ValidationGroup="RevertCompanyCredit" />
                        <asp:Button ID="btnRevertCompanyCreditclosed" runat="server" Text="Close"
                            CssClass="btn btn-primary btn-sm" />
                    </div>--%>
                </div>
            </div>
        </div>
    </div>

    <div id="MPAddInvoice" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Add Invoice</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Network Name
                                </label>
                                <asp:TextBox ID="txtNetWork" runat="server" CssClass="form-control" Style="height: 35px;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Invoice No
                                </label>
                                <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control" Style="height: 35px;"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Total Amount
                                </label>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" FilterType="Custom" runat="server"
                                        TargetControlID="txtTotalAmount" ValidChars=".0123456789">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:TextBox ID="txtTotalAmount" runat="server" CssClass="form-control" Text="0" Style="text-align: right; height: 35px;"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Invoice Date
                                </label>
                                <asp:TextBox ID="txtInvoiceDate" runat="server" CssClass="form-control" Style="height: 35px;"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender5" runat="server" DefaultView="Days" Format="MM/dd/yyyy"
                                    TargetControlID="txtInvoiceDate">
                                </asp:CalendarExtender>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Invoice Upload
                                </label>
                                <asp:FileUpload ID="FLInvoiceUpload" runat="server" CssClass="ReadOnlyText" Style="border-radius: 25px;" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Terms
                                </label>
                                <asp:DropDownList ID="ddlTerms" runat="server" CssClass="form-control">
                                    <asp:ListItem>-Select Terms-</asp:ListItem>
                                    <asp:ListItem>Net 30</asp:ListItem>
                                    <asp:ListItem>Net 60</asp:ListItem>
                                    <asp:ListItem>Net 90</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <%--<div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Paid Receipt Upload
                                </label>
                                <asp:FileUpload ID="FLPaidReceiptUpload" runat="server" CssClass="ReadOnlyText" Style="border-radius: 25px;" />
                            </div>
                        </div>--%>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Notes
                                </label>
                                <asp:TextBox ID="txtInvoiceremark" runat="server" CssClass="form-control"
                                    TextMode="MultiLine" Style="height: 50px !important; min-height: 0px !important" Rows="3" TabIndex="53"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <br />
                            <br />
                            <asp:Button ID="btnInvoiceSave" runat="server" CssClass="btn btn-success btn-sm pull-right btn-save"
                                Text="Save" OnClick="btnInvoiceSave_Click" />
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <%--  <div class="submit-section">
                        <asp:Button ID="Button5" runat="server" CssClass="btn btn-primary btn-sm"
                            Text="Revert Company Credit" OnClick="btnRevertCompanyCredit_Click" ValidationGroup="RevertCompanyCredit" />
                        <asp:Button ID="btnRevertCompanyCreditclosed" runat="server" Text="Close"
                            CssClass="btn btn-primary btn-sm" />
                    </div>--%>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function PopupOpenMPECancelOrder() {
            $('#MPECancelOrder').modal('show');
        }
        function PopupOpenMPEUpdateMedia() {
            $('#MPEUpdateMedia').modal('show');
        }
        function PopupOpenMPAddInvoice() {
            $('#MPAddInvoice').modal('show');
        }
    </script>

</asp:Content>

