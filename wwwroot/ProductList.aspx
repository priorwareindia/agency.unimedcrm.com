﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ProductList.aspx.cs" Inherits="ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table-bordered th {
            vertical-align: middle;
        }

            .table-bordered th a {
                color: #ffffff;
            }

        .text-kty, .text-kty th a {
            color: #fff !important;
        }

        tr > th {
            background-color: #36b9cc !important;
        }
        .btn-save {
        text-transform: none !important;
        letter-spacing: 0 !important;
        min-width: 0px !important;
        padding: 0.375rem 0.75rem !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800 text-center" style="width: 100%">Your Ordered Products
            <a href="Home.aspx" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm float-left btn-save">
                <i class="fas fa-arrow-left fa-sm text-white"></i><b style="padding-left: 10px;">Back</b> </a>
        </h1>

    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-2">
            <h6 class="m-0 font-weight-bold text-primary mt-1 mb-1">Your Ordered Products</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:GridView ID="GridProductLog" runat="server" AutoGenerateColumns="False" AllowSorting="True" class="table table-bordered" OnSorting="GridProductLog_Sorting" OnRowDataBound="GridProductLog_RowDataBound">
                    <Columns>

                        <asp:TemplateField HeaderText="Invoice No" SortExpression="SYSOrderNo">
                            <ItemTemplate>
                                <asp:Label ID="lnkProInvoiceNo" runat="server"
                                    Text='<%#Eval("InvoiceNo") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Wrap="False" BorderStyle="Outset" />
                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SKU ID" SortExpression="SkuID">
                            <ItemTemplate>
                                <asp:Label ID="lnkProSkuID" runat="server" Text='<%#Eval("SkuID") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Wrap="False" BorderStyle="Outset" />
                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Quantity" HeaderText="SKU Quantity" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="SKUDescrip" HeaderText="Description" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="ProductName" HeaderText="Product" ItemStyle-Width="30%" />
                        <asp:BoundField DataField="spquantity" HeaderText="Product Quantity" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date" SortExpression="InvoiceDate" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="OrderProcessingDate" HeaderText="Invoice Processing Date" SortExpression="OrderProcessingDate" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Product_Price" HeaderText="Product Price" ItemStyle-HorizontalAlign="Right" />
                        <asp:BoundField DataField="Sku_price" HeaderText="SKU Price" SortExpression="SKU Price" ItemStyle-HorizontalAlign="Right" />
                    </Columns>
                    <HeaderStyle CssClass="text-kty bg-info text-center align-middle" />
                </asp:GridView>

            </div>
        </div>
    </div>
</asp:Content>

