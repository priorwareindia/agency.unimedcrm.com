﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AutoId"] != null)
        {
            if (!IsPostBack)
            {
                lblmastercustomername.InnerText = Session["ContactPeronName"].ToString();
                lblAgencyName.InnerText = Session["AgencyName"].ToString();
                lblAgencyName1.InnerText = Session["AgencyName"].ToString();
            }
        }
        else
        {
            Session.Abandon();
            Session.Clear();
            Response.Redirect("Default.aspx");
        }
    }

    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtCurrentPassowrd.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Fill Current Password', 'error')", true);
                return;
            }
            if (txtNewPassword.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Fill New Password', 'error')", true);
                return;
            }
            if (txtConfirmPassword.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Fill Confirm Password', 'error')", true);
                return;
            }
            if (txtNewPassword.Text != txtConfirmPassword.Text)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','New Password Dones Not Match Confirm Password', 'error')", true);
                return;
            }
            PL_Login pobj = new PL_Login();
            pobj.CustId = Session["Session_UserloginID"].ToString();
            pobj.Zipcode = txtCurrentPassowrd.Text;
            pobj.NewPassword = txtNewPassword.Text;
            BL_Login.UpdateChangePassWord(pobj);
            if (!pobj.IsException)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Password changed successfully.', 'success')", true);
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','" + pobj.ExceptionMessage + "', 'error')", true);
            }
        }
        catch
        {

        }
    }

    protected void lnkLogOut_Click(object sender, EventArgs e)
    {
        PL_AgencyLogin pobj = new PL_AgencyLogin();
        pobj.AgencyAutoId = Convert.ToInt32(Session["AutoId"].ToString());
        pobj.IpAddress = HttpContext.Current.Request.UserHostAddress;
        BL_AgencyLogin.LogOut(pobj);
        Session.Abandon(); // Does nothing
        Session.Clear();
        Response.Redirect("~/Default.aspx");
    }

    protected void lnkChangePassword_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEMPEChangePassword", "PopupOpenMPEMPEChangePassword();", true);
    }
}
