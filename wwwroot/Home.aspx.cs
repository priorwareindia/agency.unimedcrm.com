﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Web.Services;
using DLLCustomerDetail;
using DLLInvoiceDetail;
using System.Globalization;
using System.IO;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["AutoId"] != null)
            {
                //lblAgencyName.InnerText = Session["AgencyName"].ToString();
                ViewState.Add("Who", Session["AutoId"].ToString());
                string now = DateTime.Now.ToString("M/d/yyyy");
                string now1 = DateTime.Now.AddDays(-2).ToString("M/d/yyyy");
                txtFromDate.Text = now1;
                txtToDate.Text = now;
                btnSearch_Click(null,null);
                btnSearchInvoiceList_Click(null, null);
            }
            else
            {
                Response.Redirect("~/Default.aspx", false);
            }
        }
    }




    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Session["AutoId"] != null)
        {
            try
            {
                PL_MediaCampaign pobj = new PL_MediaCampaign();
                if (txtToDate.Text.Trim() != "")
                {
                    pobj.ToDate = Convert.ToDateTime(txtToDate.Text);
                }
                else
                {
                    pobj.ToDate = Convert.ToDateTime("01/01/2001");
                }

                if (txtFromDate.Text.Trim() != "")
                {
                    pobj.FromDate = Convert.ToDateTime(txtFromDate.Text);
                }
                else
                {
                    pobj.FromDate = Convert.ToDateTime("01/01/2001");
                }
                pobj.AgencyAutoId = Convert.ToInt32(Session["AutoId"].ToString());
                BL_MediaCampaign.BindList(pobj);
                if (pobj.Dt != null && pobj.Dt.Rows.Count > 0)
                {
                    GridCampaign.DataSource = pobj.Dt;
                    GridCampaign.DataBind();
                }
                else
                {
                    GridCampaign.DataSource = null;
                    GridCampaign.DataBind();
                }
            }
            catch
            { }
        }
    }

    protected void ImgEdit_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ImageButton creditentryid = sender as ImageButton;
            hdnMediaCampaignAutoId.Value = creditentryid.CommandArgument.Trim();
            txtNotes.Text = "";
            txtURLUpload.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPECancelOrder", "PopupOpenMPECancelOrder();", true);
        }
        catch (Exception ex)
        {

        }
    }

    protected void ImgMediaUpdate_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ImageButton creditentryid = sender as ImageButton;
            hdnMediaCampaignAutoId.Value = creditentryid.CommandArgument.Trim();
            GridViewRow gr = (sender as ImageButton).Parent.Parent as GridViewRow;
            txtLogDate.Text = gr.Cells[4].Text;
            ddlType.SelectedValue = gr.Cells[9].Text;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEUpdateMedia", "PopupOpenMPEUpdateMedia();", true);
        }
        catch (Exception ex)
        {

        }
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        try
        {
            PL_MediaCampaign pobj = new PL_MediaCampaign();
            pobj.MediaCampaignAutoId = Convert.ToInt32(hdnMediaCampaignAutoId.Value);
            pobj.AgencyDecumentRemark = txtNotes.Text;
            pobj.AgencyAutoId = Convert.ToInt32(Session["AutoId"].ToString());
            pobj.AgencyURL = txtURLUpload.Text;

            //if (FileUpload1.HasFile)
            //{
            //    HttpFileCollection _HttpFileCollection = Request.Files;
            //    for (int i = 0; i < _HttpFileCollection.Count; i++)
            //    {
            //        HttpPostedFile _HttpPostedFile = _HttpFileCollection[i];
            //        if (_HttpPostedFile.ContentLength > 0)
            //        {
            //            string filename = System.DateTime.Now.ToString("ddmmyy_hhmmss") + "_" + Path.GetFileName(_HttpPostedFile.FileName);
            //            string ImagePath = "/GA_Website_Update_Log_Image/" + filename;
            //            string ImageFileName = Path.GetFileName(_HttpPostedFile.FileName);
            //            _HttpPostedFile.SaveAs(Server.MapPath("~/GA_Website_Update_Log_Image/" + filename));
            //            ImageXml += "<ImageXml>";
            //            ImageXml += "<ImagePath><![CDATA[" + ImagePath + "]]></ImagePath>";
            //            ImageXml += "<ImageFileName><![CDATA[" + ImageFileName + "]]></ImageFileName>";
            //            ImageXml += "</ImageXml>";
            //        }
            //    }
            //}
            //pobj.ImageXml = ImageXml;

            if (FileUpload1.HasFile)
            {
                string filename = System.DateTime.Now.ToString("ddmmyy_hhmmss") + "_" + Path.GetFileName(FileUpload1.PostedFile.FileName);
                FileUpload1.SaveAs(Server.MapPath("/AgencyMediaCompaingnDocumnet/" + filename));
                pobj.AgencyDocumentPath = "/AgencyMediaCompaingnDocumnet/" + filename;
                pobj.AgencyDocumentName = Path.GetFileName(FileUpload1.PostedFile.FileName);
            }
            else
            {
                pobj.AgencyDocumentPath = "";
                pobj.AgencyDocumentName = "";
            }
            if (pobj.AgencyDocumentPath == "" && pobj.AgencyURL=="")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Warning','Please add document or url upload.', 'warning')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPECancelOrder1", "PopupOpenMPECancelOrder();", true);
                return;
            }
            BL_MediaCampaign.UploadDocumnet(pobj);
            if (!pobj.IsException)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Document update successfully.', 'success')", true);
                btnSearch_Click(null, null);
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Document is not updated.', 'error')", true);
            }
        }
        catch
        {
        }
    }

    protected void lnkImageFileName_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = (sender as LinkButton).Parent.Parent as GridViewRow;
            string ImagePath = gr.Cells[1].Text.ToString();
            // Response.Redirect(ImagePath);

            string Url = ImagePath;
            Response.Write("<script language='javascript'>window.open('" + Url + "','_blank','');");
            Response.Write("</script>");
        }
        catch
        { }
    }


    protected void lnkAgencyURL_Click(object sender, EventArgs e)
    {
        string ImagePath = (sender as LinkButton).Text;
        // Response.Redirect(ImagePath);

        string Url = ImagePath;
        Response.Write("<script language='javascript'>window.open('" + Url + "','_blank','');");
        Response.Write("</script>");
    }

    protected void btnUpdateMedia_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtLogDate.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Warning','Please select log date.', 'warning')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEUpdateMedia1", "PopupOpenMPEUpdateMedia();", true);
                return;
            }
            if (ddlType.SelectedIndex==0)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Warning','Please select  clearance.', 'warning')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEUpdateMedia2", "PopupOpenMPEUpdateMedia();", true);
                return;
            }

            PL_MediaCampaign pobj = new PL_MediaCampaign();
            pobj.MediaCampaignAutoId = Convert.ToInt32(hdnMediaCampaignAutoId.Value);
            pobj.LogDate = Convert.ToDateTime(txtLogDate.Text);
            pobj.AgencyAutoId = Convert.ToInt32(Session["AutoId"].ToString());
            pobj.Type = ddlType.SelectedValue;
            BL_MediaCampaign.MediaUpdate(pobj);
            if (!pobj.IsException)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Media update successfully.', 'success')", true);
                btnSearch_Click(null, null);
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Document is not updated.', 'error')", true);
            }
        }
        catch
        {
        }
    }
    #region Invoice List
    protected void btnAddInvoice_Click(object sender, EventArgs e)
    {
        string now = DateTime.Now.ToString("M/d/yyyy");
        txtInvoiceNo.Text = "";
        txtInvoiceDate.Text = now;
        txtTotalAmount.Text = "0";
        txtNetWork.Text = "";
        txtInvoiceremark.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPAddInvoice", "PopupOpenMPAddInvoice();", true);
    }

    protected void btnInvoiceSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtNetWork.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Warning','Please fill netwoork name', 'warning')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPAddInvoice1", "PopupOpenMPAddInvoice();", true);
                return;
            }
            if (txtInvoiceNo.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Warning','Please fill Invoice no.', 'warning')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPAddInvoice2", "PopupOpenMPAddInvoice();", true);
                return;
            }
            if (txtInvoiceDate.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Warning','Please select invoice date.', 'warning')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPAddInvoice3", "PopupOpenMPAddInvoice();", true);
                return;
            }
            if (ddlTerms.SelectedIndex == 0)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Warning','Please select terms.', 'warning')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPAddInvoice4", "PopupOpenMPAddInvoice();", true);
                return;
            }
            PL_AgencyInvoiceMaster pobj = new PL_AgencyInvoiceMaster();
            pobj.InvoiceNo = txtInvoiceNo.Text;
            pobj.InvoiceDate = Convert.ToDateTime(txtInvoiceDate.Text);
            pobj.AgencyAutoId = Convert.ToInt32(Session["AutoId"].ToString());
            pobj.Terms = ddlTerms.SelectedValue;
            pobj.Networkname = txtNetWork.Text;
            try
            {
                pobj.TotalAmount = Convert.ToDecimal(txtTotalAmount.Text);
            }
            catch
            {
                pobj.TotalAmount = 0;
            }
            pobj.Remark = txtInvoiceremark.Text;

            if (FLInvoiceUpload.HasFile)
            {
                string filename = System.DateTime.Now.ToString("ddmmyy_hhmmss") + "_" + Path.GetFileName(FLInvoiceUpload.PostedFile.FileName);
                FLInvoiceUpload.SaveAs(Server.MapPath("/AgencyInvoiceDocument/" + filename));
                pobj.InvoiceUploadDocumentPath = "/AgencyInvoiceDocument/" + filename;
                pobj.InvoiceUploadDocumentName = Path.GetFileName(FLInvoiceUpload.PostedFile.FileName);
            }
            else
            {
                pobj.InvoiceUploadDocumentPath = "";
                pobj.InvoiceUploadDocumentName = "";
            }
            if (pobj.InvoiceUploadDocumentPath == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Warning','Please upload invoice document.', 'warning')", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPAddInvoice4", "PopupOpenMPAddInvoice();", true);
                return;
            }
            //if (FLPaidReceiptUpload.HasFile)
            //{
            //    string filename = System.DateTime.Now.ToString("ddmmyy_hhmmss") + "_" + Path.GetFileName(FLPaidReceiptUpload.PostedFile.FileName);
            //    FLPaidReceiptUpload.SaveAs(Server.MapPath("/AgencyInvoiceDocument/" + filename));
            //    pobj.PaidReceiptDocumentPath = "/AgencyInvoiceDocument/" + filename;
            //    pobj.PaidReceiptDocumentName = Path.GetFileName(FLPaidReceiptUpload.PostedFile.FileName);
            //}
            //else
            //{
            pobj.PaidReceiptDocumentPath = "";
                pobj.PaidReceiptDocumentName = "";
            //}
            BL_AgencyInvoiceMaster.Insert(pobj);
            if (!pobj.IsException)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Invoice save successfully.', 'success')", true);
                btnSearchInvoiceList_Click(null, null);
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Invoice is not save.', 'error')", true);
            }
        }
        catch
        { }
    }

    protected void lnkInvoiceDocument_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = (sender as LinkButton).Parent.Parent as GridViewRow;
            string ImagePath = gr.Cells[1].Text.ToString();
            // Response.Redirect(ImagePath);

            string Url = ImagePath;
            Response.Write("<script language='javascript'>window.open('" + Url + "','_blank','');");
            Response.Write("</script>");
        }
        catch
        { }
    }
    protected void lnkPaidreceipt_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = (sender as LinkButton).Parent.Parent as GridViewRow;
            string ImagePath = gr.Cells[2].Text.ToString();
            // Response.Redirect(ImagePath);

            string Url = ImagePath;
            Response.Write("<script language='javascript'>window.open('" + Url + "','_blank','');");
            Response.Write("</script>");
        }
        catch
        { }
    }

    protected void btnSearchInvoiceList_Click(object sender, EventArgs e)
    {
        if (Session["AutoId"] != null)
        {
            try
            {
                PL_AgencyInvoiceMaster pobj = new PL_AgencyInvoiceMaster();
                pobj.AgencyAutoId = Convert.ToInt32(Session["AutoId"].ToString());
                BL_AgencyInvoiceMaster.InvoiceList(pobj);
                if (pobj.Dt != null && pobj.Dt.Rows.Count > 0)
                {
                    grdinvoiceList.DataSource = pobj.Dt;
                    grdinvoiceList.DataBind();
                }
                else
                {
                    grdinvoiceList.DataSource = null;
                    grdinvoiceList.DataBind();
                }
            }
            catch
            { }
        }
    }

   
    #endregion
}