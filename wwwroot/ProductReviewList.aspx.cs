﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Web.Services;
using DLLCustomerDetail;
using AjaxControlToolkit;

public partial class ProductReviewList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindOrderProductGrid();
            
        }
    }

    private void bindOrderProductGrid()
    {
        try
        {
            PL_CustomerDetail pobj = new PL_CustomerDetail();
            if (Session["Session_UserloginID"].ToString() != "")
            {
                pobj.CustID = Session["Session_UserloginID"].ToString();
            }
            BL_CustomerDetail.BindProductReviewList(pobj);
            GridProductLog.DataSource = pobj.Ds.Tables[0];
            GridProductLog.DataBind();

        }
        catch { }
    }

    protected void GridProductLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            int rating1 = Convert.ToInt32(e.Row.Cells[1].Text);
            Rating rating = e.Row.FindControl("Rating1") as Rating;
            LinkButton lbtnWriteReview= e.Row.FindControl("lbtnWriteReview") as LinkButton;
            if (rating1 > 0)
            {
                rating.Visible = true;
                lbtnWriteReview.Visible = false;
                rating.CurrentRating = rating1;
            }
            else
            {
                lbtnWriteReview.Visible = true;
                rating.Visible = false;
            }
        }
        catch
        {

        }
    }

    protected void lbtnWriteReview_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            hdnProductId.Value = gr.Cells[0].Text;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPECancelOrder", "PopupOpenMPECancelOrder();", true);
        }
        catch
        {

        }
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        try
        {
            PL_CustomerDetail pobj = new PL_CustomerDetail();
            pobj.CustID = Session["Session_UserloginID"].ToString();
            pobj.ProductId = hdnProductId.Value;
            pobj.Comment = txtReview.Text;
            pobj.Rating = RatingProduct.CurrentRating;
            BL_CustomerDetail.InsertProductRating(pobj);
            if (!pobj.IsException)
            {
                bindOrderProductGrid();
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Rating save successfully.', 'success')", true);

            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Rating is not save.', 'error')", true);
            }
        }
        catch
        {

        }
    }
}