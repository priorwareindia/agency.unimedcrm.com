﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyLayerConitnousOrder
/// </summary>
public class PropertyLayerConitnousOrder : Utility
{
    public PropertyLayerConitnousOrder()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    string invoiceNo,lastinvoiceNo, status, sYSorderno,sYSordernoNew, billingEntryID, shippingEntryID, creditEntryID, custID, orderType,productId,
        comments, parentInvoiceType, skuID, custName, custStatus, paymentID, paymentMethod, sourceKeyId,invstatus;


    decimal discount, sPUnitPrice, sPQuantity, sPDiscount;
    bool Onetimeonly;

    public string TempHoldDate { get; set; }
    public decimal CreditGiven { get; set; }
    public decimal HappyPoints { get; set; }
    public decimal AHappyPointAmount { get; set; }
    public decimal Subtotal { get; set; }
    public decimal StateTax { get; set; }
    public decimal OverallDiscount { get; set; }
    public decimal Shipping { get; set; }
    public string SKUType { get; set; }
    public decimal SPDiscount
    {
        get { return sPDiscount; }
        set { sPDiscount = value; }
    }
    public string SKUXml { get; set; }
    public string SKuProductXml { get; set; }
    public decimal SPQuantity
    {
        get { return sPQuantity; }
        set { sPQuantity = value; }
    }

    public decimal SPUnitPrice
    {
        get { return sPUnitPrice; }
        set { sPUnitPrice = value; }
    }

    public decimal Discount
    {
        get { return discount; }
        set { discount = value; }
    }
    public string SourceKeyId
    {
        get { return sourceKeyId; }
        set { sourceKeyId = value; }
    }
    public string InvStatus
    {
        get { return invstatus; }
        set { invstatus = value; }
    }
public string ProductId
    {
        get { return productId; }
        set { productId = value; }
    }
    public string PaymentMethod
    {
        get { return paymentMethod; }
        set { paymentMethod = value; }
    }

    public string PaymentID
    {
        get { return paymentID; }
        set { paymentID = value; }
    }

    public string CustStatus
    {
        get { return custStatus; }
        set { custStatus = value; }
    }

    public string CreditEntryID
    {
        get { return creditEntryID; }
        set { creditEntryID = value; }
    }

    public string CustName
    {
        get { return custName; }
        set { custName = value; }
    }
    DateTime cancelDate, declineDate, lastDeclineDate, nxtshippingDate,toNxtShpdate,fromNxtShpdate,toLstShpdate,fromLstShpdate,
          prevshippingDate;

    public DateTime FromLstShpdate
    { 
        get { if (fromLstShpdate == null || fromLstShpdate == DateTime.MinValue) { fromLstShpdate = Convert.ToDateTime("01/01/2010"); } return fromLstShpdate; }
    set { if (fromLstShpdate == null || fromLstShpdate == DateTime.MinValue) { fromLstShpdate = Convert.ToDateTime("01/01/2010"); } fromLstShpdate = value; }

    }

    public DateTime ToLstShpdate
    {
         
        get { if (toLstShpdate == null || toLstShpdate == DateTime.MinValue) { toLstShpdate = Convert.ToDateTime("01/01/2010"); } return toLstShpdate; }
    set { if (toLstShpdate == null || toLstShpdate == DateTime.MinValue) { toLstShpdate = Convert.ToDateTime("01/01/2010"); } toLstShpdate = value; }

    }

    public DateTime FromNxtShpdate
    { 
        get { if (fromNxtShpdate == null || fromNxtShpdate == DateTime.MinValue) { fromNxtShpdate = Convert.ToDateTime("01/01/2010"); } return fromNxtShpdate; }
    set { if (fromNxtShpdate == null || fromNxtShpdate == DateTime.MinValue) { fromNxtShpdate = Convert.ToDateTime("01/01/2010"); } fromNxtShpdate = value; }

    }

    public DateTime ToNxtShpdate
    { 
         get { if (toNxtShpdate == null || toNxtShpdate == DateTime.MinValue) { toNxtShpdate = Convert.ToDateTime("01/01/2010"); } return toNxtShpdate; }
    set { if (toNxtShpdate == null || toNxtShpdate == DateTime.MinValue) { toNxtShpdate = Convert.ToDateTime("01/01/2010"); } toNxtShpdate = value; }

    }
    Decimal quantity, unitPrice;
    int shippingInterval, declinecount, reShippingNo;

    public DateTime PrevshippingDate
    {
        
         get { if (prevshippingDate == null || prevshippingDate == DateTime.MinValue) { prevshippingDate = Convert.ToDateTime("01/01/2010"); } return prevshippingDate; }
    set { if (prevshippingDate == null || prevshippingDate == DateTime.MinValue) { prevshippingDate = Convert.ToDateTime("01/01/2010"); } prevshippingDate = value; }

    }

    public DateTime NxtshippingDate
    { 
         get { if (nxtshippingDate == null || nxtshippingDate == DateTime.MinValue) { nxtshippingDate = Convert.ToDateTime("01/01/2010"); } return nxtshippingDate; }
    set { if (nxtshippingDate == null || nxtshippingDate == DateTime.MinValue) { nxtshippingDate = Convert.ToDateTime("01/01/2010"); } nxtshippingDate = value; }

    }

    public Decimal UnitPrice
    {
        get { return unitPrice; }
        set { unitPrice = value; }
    }

    public Decimal Quantity
    {
        get { return quantity; }
        set { quantity = value; }
    }

    public int Declinecount
    {
        get { return declinecount; }
        set { declinecount = value; }
    }

    public int ShippingInterval
    {
        get { return shippingInterval; }
        set { shippingInterval = value; }
    }

    public int ReShippingNo
    {
        get { return reShippingNo; }
        set { reShippingNo = value; }
    }
    public DateTime LastDeclineDate
    {
        get { return lastDeclineDate; }
        set { lastDeclineDate = value; }
    }

    public DateTime DeclineDate
    {
        get { return declineDate; }
        set { declineDate = value; }
    }

    public DateTime CancelDate
    {
        get { return cancelDate; }
        set { cancelDate = value; }
    }



    public string SkuID
    {
        get { return skuID; }
        set { skuID = value; }
    }


    public string ParentInvoiceType
    {
        get { return parentInvoiceType; }
        set { parentInvoiceType = value; }
    }



    public string Comments
    {
        get { return comments; }
        set { comments = value; }
    }



    public string OrderType
    {
        get { return orderType; }
        set { orderType = value; }
    }

    public string CustID
    {
        get { return custID; }
        set { custID = value; }
    }

    public string ShippingEntryID
    {
        get { return shippingEntryID; }
        set { shippingEntryID = value; }
    }

    public string BillingEntryID
    {
        get { return billingEntryID; }
        set { billingEntryID = value; }
    }

    public string SYSorderno
    {
        get { return sYSorderno; }
        set { sYSorderno = value; }
    }
    public string SYSordernoNew
    {
        get { return sYSordernoNew; }
        set { sYSordernoNew = value; }
    }
   // public int Status { get; set; }
    public string Status
    {
        get { return status; }
        set { status = value; }
    }

    public string InvoiceNo
    {
        get { return invoiceNo; }
        set { invoiceNo = value; }
    }

    public string LastInvoiceNo
    {
        get { return lastinvoiceNo; }
        set { lastinvoiceNo = value; }
    }
    public bool onetimeonly
    {
        get { return Onetimeonly; }
        set { Onetimeonly = value; }
    }
}