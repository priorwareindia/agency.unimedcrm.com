﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class PL_MediaCampaign : Utility
{
    public int AgencyAutoId { get; set; }
    public int MediaCampaignAutoId { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string AgencyDocumentPath { get; set; }
    public string AgencyDocumentName { get; set; }
    public string AgencyDecumentRemark { get; set; }
    public string AgencyURL { get; set; }
    public string Type { get; set; }
    public DateTime LogDate { get; set; }
}
public class DL_MediaCampaign
{
    public static void returnTable(PL_MediaCampaign pobj)
    {
        try
        {
            DataBaseUtilitys dobj = new DataBaseUtilitys();
            SqlCommand sqlCmd = new SqlCommand("proc_AgencyPortal_MediaCampaign", dobj.SqlCon);
            sqlCmd.CommandTimeout = 10000;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
            sqlCmd.Parameters.AddWithValue("@AgencyAutoId", pobj.AgencyAutoId);
            sqlCmd.Parameters.AddWithValue("@MediaCampaignAutoId", pobj.MediaCampaignAutoId);
            sqlCmd.Parameters.AddWithValue("@AgencyDocumentPath", pobj.AgencyDocumentPath);
            sqlCmd.Parameters.AddWithValue("@AgencyDocumentName", pobj.AgencyDocumentName);
            sqlCmd.Parameters.AddWithValue("@AgencyDecumentRemark", pobj.AgencyDecumentRemark);
            sqlCmd.Parameters.AddWithValue("@AgencyURL", pobj.AgencyURL);
            sqlCmd.Parameters.AddWithValue("@Type", pobj.Type);
            if (pobj.OpCode == 41)
            {
                sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);

            }
            if (pobj.OpCode == 22)
            {
                sqlCmd.Parameters.AddWithValue("@LogDate", pobj.LogDate);
            }

            sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar, 25).Value = pobj.Who;
            sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
            sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.pageNo;

            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            pobj.Ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlCmd;
            da.Fill(pobj.Ds);
            if (pobj.Ds != null && pobj.Ds.Tables.Count != 0)
            {
                pobj.Dt = pobj.Ds.Tables[0];
            }
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception ex)
        {
        }
    }

}
public class BL_MediaCampaign
{

    public static void BindList(PL_MediaCampaign pobj)
    {
        pobj.OpCode = 41;
        DL_MediaCampaign.returnTable(pobj);
    }
    public static void UploadDocumnet(PL_MediaCampaign pobj)
    {
        pobj.OpCode = 21;
        DL_MediaCampaign.returnTable(pobj);
    }
    public static void MediaUpdate(PL_MediaCampaign pobj)
    {
        pobj.OpCode = 22;
        DL_MediaCampaign.returnTable(pobj);
    }
}