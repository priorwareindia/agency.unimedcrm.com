﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

public class YotpoAPI
{

    public static String GetTimestamp(DateTime value)
    {
        return value.ToString("yyyyMMddHHmmssffff");

       
    }

    #region Test Credential
    //request.Headers.Add("x-api-key", "f374rng9p9iRs05ccixVUQtt");
    //    request.Headers.Add("x-guid", "-F04W5Huk5TRzJNIOV-hDg");
    #endregion
    #region Live Credential
    //request.Headers.Add("x-api-key", "3Wm9wog2R1O61rwEymr8cwtt");
    //        request.Headers.Add("x-guid", "BfTAg5zxIGaze1xi9nihuA");
    #endregion
    #region Get Points
    public static string FetchCustomerYotpoPoints(string customer_email)
    {


        try
        {
            string URL = "https://loyalty.yotpo.com/api/v2/customers?customer_email=" + customer_email + "&country_iso_code=null&with_referral_code=false&with_history=true";
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Headers.Add("x-api-key", "3Wm9wog2R1O61rwEymr8cwtt");
            request.Headers.Add("x-guid", "BfTAg5zxIGaze1xi9nihuA");
            try
            {
                // get the response
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                responseReader.Close();
                RootObject instance = JsonConvert.DeserializeObject<RootObject>(response);
                if (instance != null)
                {
                    return instance.points_balance;
                }
                else
                {
                    return "";
                }
                //return response;
            }
            catch (WebException we)
            {
                //lbltxt.Text = we.Message;
                return "";
            }

        }
        catch (System.Exception ex)
        {
            return "";
        }
    }
    #endregion
    #region Create order(Add Points)
    public static string CreateOrder_YotpoPoints(string customer_email, int Amount, string InvoiceNo)
    {
        try
        {

            var rand = new Random();
            int number = rand.Next(1, 9999999);
            Resquest obj = new Resquest();
            obj.customer_email = customer_email;
            obj.currency_code = "USD";
            obj.total_amount_cents = Amount;
            obj.order_id = InvoiceNo;//GetTimestamp(DateTime.Now);//Convert.ToString(number);

            string data = new JavaScriptSerializer().Serialize(obj);

            string URL = "https://loyalty.yotpo.com/api/v2/orders";
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("x-api-key", "3Wm9wog2R1O61rwEymr8cwtt");
            request.Headers.Add("x-guid", "BfTAg5zxIGaze1xi9nihuA");
            request.ContentLength = data.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(data);
            requestWriter.Close();
            try
            {
                // get the response
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                responseReader.Close();
                //RootObject instance = JsonConvert.DeserializeObject<RootObject>(response);
                //if (instance != null)
                //{
                //    return instance.points_balance;
                //}
                //else
                //{
                    return "";
                //}
                //return response;
            }
            catch (WebException we)
            {
                //lbltxt.Text = we.Message;
                return "";
            }

        }
        catch (System.Exception ex)
        {
            return "";
        }
    }
    #endregion

    #region Get Active Redemption Options
    public static string FetchRedeemOptionYotpo()
    {


        try
        {
            string URL = "https://loyalty.yotpo.com/api/v2/redemption_options";
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Headers.Add("x-api-key", "3Wm9wog2R1O61rwEymr8cwtt");
            request.Headers.Add("x-guid", "BfTAg5zxIGaze1xi9nihuA");
            try
            {
                // get the response
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                responseReader.Close();
                return response;
                //return response;
            }
            catch (WebException we)
            {
                //lbltxt.Text = we.Message;
                return "";
            }

        }
        catch (System.Exception ex)
        {
            return "";
        }
    }
    #endregion
    #region Create Redemption 
    public static string CreateRedeem_YotpoPoints(string customer_email, int redemption_option_id)
    {
        try
        {

            var rand = new Random();
            int number = rand.Next(1, 9999999);
            ResquestRedeem obj = new ResquestRedeem();
            obj.customer_email = customer_email;
            obj.redemption_option_id = redemption_option_id;
           
            string data = new JavaScriptSerializer().Serialize(obj);

            string URL = "https://loyalty.yotpo.com/api/v2/redemptions";
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("x-api-key", "3Wm9wog2R1O61rwEymr8cwtt");
            request.Headers.Add("x-guid", "BfTAg5zxIGaze1xi9nihuA");
            request.ContentLength = data.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(data);
            requestWriter.Close();
            try
            {
                // get the response
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                responseReader.Close();
                ResponseRedeem instance = JsonConvert.DeserializeObject<ResponseRedeem>(response);
                if (instance != null)
                {
                    if (instance.errors != null)
                    {
                        return instance.errors.redemption_option;
                    }
                    else if (instance.reward_text != null)
                    {
                        //CreateOrderRedeem_YotpoPoints(customer_email, Amount, instance.reward_text);
                        return instance.reward_text;
                    }
                    else
                    {
                        return "You don't have enough points for this amount";
                    }
                }
                else
                {
                    return "Not Found";
                }
                //return response;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Unprocessable Entity."))
                {
                    return "You don't have enough points for this amount";
                }
                else
                {
                    return "Not Found";
                }
                //lbltxt.Text = we.Message;
               
            }

        }
        catch (System.Exception ex)
        {
            return "";
        }
    }

    public static string CreateOrderRedeem_YotpoPoints(string customer_email, int Amount,string coupon_code)
    {
        try
        {

            var rand = new Random();
            int number = rand.Next(1, 9999999);
            ResquestRedeemOrder obj = new ResquestRedeemOrder();
            obj.customer_email = customer_email;
            obj.currency_code = "USD";
            obj.coupon_code = coupon_code;
            obj.total_amount_cents = Amount;
            obj.order_id = GetTimestamp(DateTime.Now);//Convert.ToString(number);

            string data = new JavaScriptSerializer().Serialize(obj);

            string URL = "https://loyalty.yotpo.com/api/v2/orders";
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("x-api-key", "3Wm9wog2R1O61rwEymr8cwtt");
            request.Headers.Add("x-guid", "BfTAg5zxIGaze1xi9nihuA");
            request.ContentLength = data.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(data);
            requestWriter.Close();
            try
            {
                // get the response
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                responseReader.Close();
                //RootObject instance = JsonConvert.DeserializeObject<RootObject>(response);
                //if (instance != null)
                //{
                //    return instance.points_balance;
                //}
                //else
                //{
                return "";
                //}
                //return response;
            }
            catch (WebException we)
            {
                //lbltxt.Text = we.Message;
                return "";
            }

        }
        catch (System.Exception ex)
        {
            return "";
        }
    }
    #endregion
}

public class RootObject
{
    public string points_balance { get; set; }
}

public class Resquest
{
    public string customer_email;
    public int total_amount_cents;
    public string currency_code;
    public string order_id;
}

public class ResquestRedeemOrder
{
    public string customer_email;
    public int total_amount_cents;
    public string currency_code;
    public string coupon_code;
    public string order_id;
}

public class ResquestRedeem
{
    public string customer_email;
    public int redemption_option_id;
}

public class ResponseRedeem
{
    public errors errors { get; set; }
    public int id { get; set; }
    public string reward_text { get; set; }
   
}
public class errors
{
    public string redemption_option { get; set; }
}

