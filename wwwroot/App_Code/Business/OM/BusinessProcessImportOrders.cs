﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyBusinessImportOrders
/// </summary>
public class BusinessProcessImportOrders
{
   
    public static void ProcessAPiOrder(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 12;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updateCancelOrder(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 21;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updateCancelBackOrder(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 22;
        DataProcessImportOrders.returnTable(pobj);
    }

    #region Show Cancel Button
    public static void updateCancelInvoice(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 23;
        DataProcessImportOrders.returnTable(pobj);
    }
    #endregion
    public static void updateCustomerProfileId(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 24;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updateCustomerPaymentProfileId(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 25;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updateShippingSouceKey(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 26;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updateShipping(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 27;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updatehappypoint(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 28;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updatorderholddate(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 29;
        DataProcessImportOrders.returnTable(pobj);
    }  
    public static void updateSouceKeyInv(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 201;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updateSouceKeyConti(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 202;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updatinvoiceholddate(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 30;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void updateprocessunlock(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 31;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectallProcessgrid(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 41;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectorderbyID(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 42;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectGridorderbyID(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 43;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectGridPrintbyID(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 431;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectdtOrg(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 433;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectInvoiceno(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 44;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectProcessgridbyCustID(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 45;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectProcessgridBYdate(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 46;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectItembyUsePrice(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 48;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectShippingbyID(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 49;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void addSpecialShipping(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 401;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void selectSaleTax(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 402;
        DataProcessImportOrders.returnTable(pobj);
    }

    public static void SelectPShipping(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 403;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void calculateHappypoints(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 404;
        DataProcessImportOrders.returnTable(pobj);
    }

    public static void searchByParameters(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 61;
        DataProcessImportOrders.returnTable(pobj);
    }
    
    public static void searchBOByParameters(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 62;
        DataProcessImportOrders.returnTable(pobj);
    }
    
    public static void selectBackOrderGrid(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 432;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void CountAlertBackOrder(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 51;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void BindMarketinReport(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 64;
        DataProcessImportOrders.returnTable(pobj);
    }

    public static void BindSaleSource(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 65;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void BindUserDetails(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 66;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void BindCategory(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 67;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void BindMarketinReportB(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 68;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void Autoshipreport(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 69;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void OrderSourcereport(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 70;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void Autoshipreport2(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 71;
        DataProcessImportOrders.returnTable(pobj);
    }
    public static void BindCustomerList(PropertyProcessImportOrders pobj)
    {
        pobj.OpCode = 76;
        DataProcessImportOrders.returnTable(pobj);
    }

}