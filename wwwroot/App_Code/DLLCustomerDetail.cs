﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace DLLCustomerDetail
{
    public class PL_CustomerDetail : Utility
    {
        public string CustID { get; set; }
        public string Prefix { get; set; }
        public string countryAbbr { get; set; }
        public string stateAbbr { get; set; }
        public string zipcode { get; set; }
        public string SAddress1 { get; set; }
        public string SAddress2 { get; set; }
        public string SCity { get; set; }
        public string SState { get; set; }
        public string SZip { get; set; }
        public string SCountry { get; set; }
        public string SPhone { get; set; }
        public string SPhone1 { get; set; }
        public string OfficeNoS { get; set; }
        public string SEmailID { get; set; }
        public string STCompany { get; set; }
        public string ShippingEntryID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
        public string ProductId { get; set; }
    }

    public class DL_CustomerDetail
    {
        public static void returnTable(PL_CustomerDetail pobj)
        {
            try
            {
                DataBaseUtilitys dobj = new DataBaseUtilitys();
                SqlCommand sqlCmd = new SqlCommand("Proc_BackOffice_CustomerDetail", dobj.SqlCon);
                sqlCmd.CommandTimeout = 500;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@OpCode", pobj.OpCode);
                sqlCmd.Parameters.AddWithValue("@CustID", pobj.CustID);
                sqlCmd.Parameters.AddWithValue("@Prefix", pobj.Prefix);
                sqlCmd.Parameters.AddWithValue("@countryAbbr", pobj.countryAbbr);
                sqlCmd.Parameters.AddWithValue("@stateAbbr", pobj.stateAbbr);
                sqlCmd.Parameters.AddWithValue("@zipcode", pobj.zipcode);
                sqlCmd.Parameters.AddWithValue("@SAddress1", pobj.SAddress1);
                sqlCmd.Parameters.AddWithValue("@SAddress2", pobj.SAddress2);
                sqlCmd.Parameters.AddWithValue("@SCity", pobj.SCity);
                sqlCmd.Parameters.AddWithValue("@SState", pobj.SState);
                sqlCmd.Parameters.AddWithValue("@SZip", pobj.SZip);
                sqlCmd.Parameters.AddWithValue("@SCountry", pobj.SCountry);
                sqlCmd.Parameters.AddWithValue("@SPhone", pobj.SPhone);
                sqlCmd.Parameters.AddWithValue("@SPhone1", pobj.SPhone1);
                sqlCmd.Parameters.AddWithValue("@OfficeNoS", pobj.OfficeNoS);
                sqlCmd.Parameters.AddWithValue("@SEmailID", pobj.SEmailID);
                sqlCmd.Parameters.AddWithValue("@STCompany", pobj.STCompany);
                sqlCmd.Parameters.AddWithValue("@FirstName", pobj.FirstName);
                sqlCmd.Parameters.AddWithValue("@LastName", pobj.LastName);
                sqlCmd.Parameters.AddWithValue("@ShippingEntryID", pobj.ShippingEntryID);
                sqlCmd.Parameters.AddWithValue("@Rating", pobj.Rating);
                sqlCmd.Parameters.AddWithValue("@Comment", pobj.Comment);
                sqlCmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.pageNo);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@Who", pobj.Who);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
               
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);


                pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
                
            }
            catch (Exception ex)
            {
                pobj.IsException = true;
                pobj.ExceptionMessage = ex.Message;
            }
        }
    }

    public class BL_CustomerDetail
    {
        public static void selectCustomerdetails(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 41;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void BindShippingAddressList(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 42;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void selectZipCodeforauto(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 43;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void selectCountry(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 44;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void selectStateByCountry(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 45;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void selectCityZIPByState(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 46;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void selectDetailsByzipcode(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 47;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void BindProductReviewList(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 48;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void InsertShippingAddress(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 11;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void updateshippop(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 12;
            DL_CustomerDetail.returnTable(pobj);
        }
        public static void InsertProductRating(PL_CustomerDetail pobj)
        {
            pobj.OpCode = 13;
            DL_CustomerDetail.returnTable(pobj);
        }
    }
}