﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;
public class PL_Login : Utility
{
    public string EmailID { get; set; }
    public string Zipcode { get; set; }
    public string IpAddress { get; set; }
    public string Latitude { get; set; }
    public string Longitude { get; set; }
    public string CustId { get; set; }
    public string NewPassword { get; set; }
    public string ActionDesc { get; set; }
    public string RefNo { get; set; }
    public DateTime Old_NxtshippingDate { get; set; }
    public DateTime New_NxtshippingDate { get; set; }
    public int Old_ShippingInterval { get; set; }
    public int New_ShippingInterval { get; set; }
    public decimal HappyPointsAmt { get; set; }
    public decimal CreditGivenAmt { get; set; }
}

public class DL_Login
{
    public static void returnTable(PL_Login pobj)
    {
        try
        {
            DataBaseUtilitys dobj = new DataBaseUtilitys();
            SqlCommand sqlCmd = new SqlCommand("Proc_Customer_Login", dobj.SqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandTimeout = 1000;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
            sqlCmd.Parameters.AddWithValue("@email", pobj.EmailID);
            sqlCmd.Parameters.AddWithValue("@CustId", pobj.CustId);
            sqlCmd.Parameters.AddWithValue("@zip", pobj.Zipcode);
            sqlCmd.Parameters.AddWithValue("@NewPassword", pobj.NewPassword);
            sqlCmd.Parameters.AddWithValue("@ActionDesc", pobj.ActionDesc);
            sqlCmd.Parameters.AddWithValue("@RefNo", pobj.RefNo);
            sqlCmd.Parameters.AddWithValue("@Old_ShippingInterval", pobj.Old_ShippingInterval);
            sqlCmd.Parameters.AddWithValue("@New_ShippingInterval", pobj.New_ShippingInterval);
            sqlCmd.Parameters.AddWithValue("@HappyPointsAmt", pobj.HappyPointsAmt);
            sqlCmd.Parameters.AddWithValue("@CreditGivenAmt", pobj.CreditGivenAmt);
            if (pobj.OpCode == 13)
            {
                sqlCmd.Parameters.AddWithValue("@Old_NxtshippingDate", pobj.Old_NxtshippingDate);
                sqlCmd.Parameters.AddWithValue("@New_NxtshippingDate", pobj.New_NxtshippingDate);
            }
            sqlCmd.Parameters.Add("@IpAddress", SqlDbType.NVarChar, 50).Value = pobj.IpAddress;
            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
            SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
            pobj.Dt = new DataTable();
            sqlAdp.Fill(pobj.Dt);
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception e)
        { }
    }
}

public class BL_Login
{
    public static void select(PL_Login pobj)
    {
        pobj.OpCode = 41;
        DL_Login.returnTable(pobj);
    }
    public static void selectCheckPasswordChange(PL_Login pobj)
    {
        pobj.OpCode = 42;
        DL_Login.returnTable(pobj);
    }
    public static void InsertLogOut(PL_Login pobj)
    {
        pobj.OpCode = 11;
        DL_Login.returnTable(pobj);
    }
    public static void UpdateChangePassWord(PL_Login pobj)
    {
        pobj.OpCode = 21;
        DL_Login.returnTable(pobj);
    }
    public static void InsertCustomerActivity(PL_Login pobj)
    {
        pobj.OpCode = 13;
        DL_Login.returnTable(pobj);
    }
    public static void ForgotPassword(PL_Login pobj)
    {
        pobj.OpCode = 14;
        DL_Login.returnTable(pobj);
    }
}