﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class PL_AgencyInvoiceMaster : Utility
{
    public int AgencyAutoId { get; set; }
    public string InvoiceNo { get; set; }
    public DateTime InvoiceDate { get; set; }
    public decimal TotalAmount { get; set; }
    public string Networkname { get; set; }
    public string InvoiceUploadDocumentPath { get; set; }
    public string InvoiceUploadDocumentName { get; set; }
    public string PaidReceiptDocumentPath { get; set; }
    public string PaidReceiptDocumentName { get; set; }
    public string Remark { get; set; }
    public string Terms { get; set; }
    public int AutoId { get; set; }
}
public class DL_AgencyInvoiceMaster
{
    public static void returnTable(PL_AgencyInvoiceMaster pobj)
    {
        try
        {
            DataBaseUtilitys dobj = new DataBaseUtilitys();
            SqlCommand sqlCmd = new SqlCommand("proc_AgencyInvoiceMaster", dobj.SqlCon);
            sqlCmd.CommandTimeout = 10000;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
            sqlCmd.Parameters.AddWithValue("@AgencyAutoId", pobj.AgencyAutoId);
            sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
            sqlCmd.Parameters.AddWithValue("@InvoiceNo", pobj.InvoiceNo);
            sqlCmd.Parameters.AddWithValue("@Terms", pobj.Terms);
            sqlCmd.Parameters.AddWithValue("@TotalAmount", pobj.TotalAmount);
            sqlCmd.Parameters.AddWithValue("@Networkname", pobj.Networkname);
            sqlCmd.Parameters.AddWithValue("@InvoiceUploadDocumentPath", pobj.InvoiceUploadDocumentPath);
            sqlCmd.Parameters.AddWithValue("@InvoiceUploadDocumentName", pobj.InvoiceUploadDocumentName);
            sqlCmd.Parameters.AddWithValue("@PaidReceiptDocumentPath", pobj.PaidReceiptDocumentPath);
            sqlCmd.Parameters.AddWithValue("@PaidReceiptDocumentName", pobj.PaidReceiptDocumentName);
            sqlCmd.Parameters.AddWithValue("@Remark", pobj.Remark);
            if (pobj.OpCode == 11)
            {
                sqlCmd.Parameters.AddWithValue("@InvoiceDate", pobj.InvoiceDate);

            }
            
            sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar, 25).Value = pobj.Who;
            sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
            sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.pageNo;

            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            pobj.Ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlCmd;
            da.Fill(pobj.Ds);
            if (pobj.Ds != null && pobj.Ds.Tables.Count != 0)
            {
                pobj.Dt = pobj.Ds.Tables[0];
            }
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception ex)
        {
        }
    }

}
public class BL_AgencyInvoiceMaster
{

    public static void Insert(PL_AgencyInvoiceMaster pobj)
    {
        pobj.OpCode = 11;
        DL_AgencyInvoiceMaster.returnTable(pobj);
    }
    public static void InvoiceList(PL_AgencyInvoiceMaster pobj)
    {
        pobj.OpCode = 41;
        DL_AgencyInvoiceMaster.returnTable(pobj);
    }
}