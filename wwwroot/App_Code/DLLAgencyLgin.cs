﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class PL_AgencyLogin : Utility
{
    public int AgencyAutoId { get; set; }
    public string IpAddress { get; set; }
    public string LoginID { get; set; }
    public string Password { get; set; }
}
public class DL_AgencyLogin
{
    public static void returnTable(PL_AgencyLogin pobj)
    {
        try
        {
            DataBaseUtilitys dobj = new DataBaseUtilitys();
            SqlCommand sqlCmd = new SqlCommand("procAgencyLogin", dobj.SqlCon);
            sqlCmd.CommandTimeout = 10000;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
            sqlCmd.Parameters.AddWithValue("@AgencyAutoId", pobj.AgencyAutoId);
            sqlCmd.Parameters.AddWithValue("@Password", pobj.Password);
            sqlCmd.Parameters.AddWithValue("@LoginID", pobj.LoginID);
            sqlCmd.Parameters.AddWithValue("@IpAddress", pobj.IpAddress);

            sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar, 25).Value = pobj.Who;
            sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
            sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.pageNo;

            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            pobj.Ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlCmd;
            da.Fill(pobj.Ds);
            if (pobj.Ds != null && pobj.Ds.Tables.Count != 0)
            {
                pobj.Dt = pobj.Ds.Tables[0];
            }
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception ex)
        {
        }
    }

}
public class BL_AgencyLogin
{

    public static void Login(PL_AgencyLogin pobj)
    {
        pobj.OpCode = 41;
        DL_AgencyLogin.returnTable(pobj);
    }
    public static void LogOut(PL_AgencyLogin pobj)
    {
        pobj.OpCode = 42;
        DL_AgencyLogin.returnTable(pobj);
    }

}
