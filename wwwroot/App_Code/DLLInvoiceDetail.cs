﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace DLLInvoiceDetail
{
    public class PL_InvoiceDetail : Utility
    {
        public string CustID { get; set; }
        public string SysOrderNo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Subject { get; set; }
        public string Reason { get; set; }
        public string Body { get; set; }
        public string Prodcode { get; set; }
        public string ProductName { get; set; }
        public int ReshipLogNo { get; set; }
        public int AutoId { get; set; }
        public string SKuProductXml { get; set; }
        public decimal HappyPoints { get; set; }
        public decimal AHappyPointAmount { get; set; }
        public decimal CreditGiven { get; set; }
        public string DateOfBirth { get; set; }
        public string ShippingEntryID { get; set; }
        public string Yotpo_Redeem { get; set; }
        public int Yotpo_Redeem_Id { get; set; }
        public int Yotpo_Redeem_Points { get; set; }
        public int Yotpo_Redeem_Amount { get; set; }
        public string Yotpo_Redeem_Code { get; set; }
    }

    public class DL_InvoiceDetail
    {
        public static void returnTable(PL_InvoiceDetail pobj)
        {
            try
            {
                DataBaseUtilitys dobj = new DataBaseUtilitys();
                SqlCommand sqlCmd = new SqlCommand("Proc_BackOffice_InvoiceMaster", dobj.SqlCon);
                sqlCmd.CommandTimeout = 500;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@OpCode", pobj.OpCode);
                sqlCmd.Parameters.AddWithValue("@CustID", pobj.CustID);
                sqlCmd.Parameters.AddWithValue("@SysOrderNo", pobj.SysOrderNo);
                sqlCmd.Parameters.AddWithValue("@Name", pobj.Name);
                sqlCmd.Parameters.AddWithValue("@Email", pobj.Email);
                sqlCmd.Parameters.AddWithValue("@Phone", pobj.Phone);
                sqlCmd.Parameters.AddWithValue("@Subject", pobj.Subject);
                sqlCmd.Parameters.AddWithValue("@Reason", pobj.Reason);
                sqlCmd.Parameters.AddWithValue("@Body", pobj.Body);
                sqlCmd.Parameters.AddWithValue("@Prodcode", pobj.Prodcode);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@ReshipLogNo", pobj.ReshipLogNo);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@SKuProductXml", pobj.SKuProductXml);
                sqlCmd.Parameters.AddWithValue("@HappyPoints", pobj.HappyPoints);
                sqlCmd.Parameters.AddWithValue("@AHappyPointAmount", pobj.AHappyPointAmount);
                sqlCmd.Parameters.AddWithValue("@CreditGiven", pobj.CreditGiven);
                sqlCmd.Parameters.AddWithValue("@DateOfBirth", pobj.DateOfBirth);
                sqlCmd.Parameters.AddWithValue("@ShippingEntryID", pobj.ShippingEntryID);
                sqlCmd.Parameters.AddWithValue("@Yotpo_Redeem", pobj.Yotpo_Redeem);
                sqlCmd.Parameters.AddWithValue("@Yotpo_Redeem_Id", pobj.Yotpo_Redeem_Id);
                sqlCmd.Parameters.AddWithValue("@Yotpo_Redeem_Points", pobj.Yotpo_Redeem_Points);
                sqlCmd.Parameters.AddWithValue("@Yotpo_Redeem_Amount", pobj.Yotpo_Redeem_Amount);
                sqlCmd.Parameters.AddWithValue("@Yotpo_Redeem_Code", pobj.Yotpo_Redeem_Code) ;
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.pageNo);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@Who", pobj.Who);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
              
            }
            catch (Exception ex)
            {
                pobj.IsException = true;
                pobj.ExceptionMessage = ex.Message;
            }
        }
    }

    public class BL_InvoiceDetail
    {
        public static void selectinvGridbyCustID(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 41;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void SearchContInv(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 42;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void selectProductSbyCustId(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 43;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void BindProCodeProName(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 44;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void BindViewInvoice(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 45;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void BindResipProduct(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 46;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void BindHappyPoints(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 47;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void BindPendingHappyPoints(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 48;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void BindPendingCredit(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 49;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void CancelOrderRqst(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 11;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void InsertContinuousOrderProduct(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 12;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void DeleteResipProduct(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 22;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void UpdateHP(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 23;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void UpdateCredit(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 24;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void UpdateDOB(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 25;
            DL_InvoiceDetail.returnTable(pobj);
        }
        public static void UpdateShipAddress(PL_InvoiceDetail pobj)
        {
            pobj.OpCode = 26;
            DL_InvoiceDetail.returnTable(pobj);
        }
    }
}