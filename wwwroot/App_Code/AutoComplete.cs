// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Public License.
// See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
// All other rights reserved.


using System;
using System.Collections.Generic;
using System.Web.Services;
using DLLCustomerDetail;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AutoComplete : WebService
{
  
   
    [WebMethod(EnableSession = true)]
    public string[] GetZipCodeCompletionList(string prefixText, int count)
    {
        if (count == 0)
        {
            count = 10;
        }
        PL_CustomerDetail pobj = new PL_CustomerDetail();
        pobj.Prefix = prefixText;
        BL_CustomerDetail.selectZipCodeforauto(pobj);


        List<string> items = new List<string>(count);
        foreach (System.Data.DataRow dr in pobj.Ds.Tables[0].Rows)
        {
            items.Add(dr[0].ToString());
        }

        return items.ToArray();
    }
  
}