﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class PL_AgencyMaster : Utility
{
    public int ID { get; set; }
    public string AgencyName { get; set; }
    public string LoginID { get; set; }
    public string Password { get; set; }
    public string AgencyStatus { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Phone { get; set; }
    public string EmailId { get; set; }
    public decimal AgencyDiscount { get; set; }
}
public class DL_AgencyMaster
{
    public static void returnTable(PL_AgencyMaster pobj)
    {
        try
        {
            DataBaseUtilitys dobj = new DataBaseUtilitys();
            SqlCommand sqlCmd = new SqlCommand("procAgencyMaster", dobj.SqlCon);
            sqlCmd.CommandTimeout = 10000;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
            sqlCmd.Parameters.AddWithValue("@ID", pobj.ID);
            sqlCmd.Parameters.AddWithValue("@AgencyName", pobj.AgencyName);
            sqlCmd.Parameters.AddWithValue("@AgencyDiscount", pobj.AgencyDiscount);
            sqlCmd.Parameters.AddWithValue("@Password", pobj.Password);
            sqlCmd.Parameters.AddWithValue("@LoginID", pobj.LoginID);
            sqlCmd.Parameters.AddWithValue("@AgencyStatus", pobj.AgencyStatus);
            sqlCmd.Parameters.AddWithValue("@FirstName", pobj.FirstName);
            sqlCmd.Parameters.AddWithValue("@LastName", pobj.LastName);
            sqlCmd.Parameters.AddWithValue("@Phone", pobj.Phone);
            sqlCmd.Parameters.AddWithValue("@EmailId", pobj.EmailId);

            sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar, 25).Value = pobj.Who;
            sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
            sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.pageNo;

            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            pobj.Ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlCmd;
            da.Fill(pobj.Ds);
            if (pobj.Ds != null && pobj.Ds.Tables.Count != 0)
            {
                pobj.Dt = pobj.Ds.Tables[0];
            }
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception ex)
        {
        }
    }

}
public class BL_AgencyMaster
{

    public static void Login(PL_AgencyMaster pobj)
    {
        pobj.OpCode = 43;
        DL_AgencyMaster.returnTable(pobj);
    }
   
}
