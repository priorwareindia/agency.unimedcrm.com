﻿using System.Data.SqlClient;
using System.Configuration;
using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;

public class Utility
{
    public int PageNo { get; set; }
    public int PageSize { get; set; }
    public int TotalRecords { get; set; }
    public int TotalPage { get; set; }
    public int StartIndex { get; set; }
    public int EndIndex { get; set; }
    public int OpCode { get; set; }
    public DataTable Dt { get; set; }
    public DataSet Ds { get; set; }
    public bool IsException { get; set; }
    public string ExceptionMessage { get; set; }

    public int pageNo { get; set; }
    public string Who { get; set; }
    public string TokenNo { get; set; }
}

public class DataBaseUtilitys
{
    public SqlConnection SqlCon = new SqlConnection(WebConfigurationManager.ConnectionStrings["appConnectionString"].ToString());

}

public class DataUtility : SQLConnectivity
{

    public string GenID(string Code)
    {
        SqlCmd = new SqlCommand("SELECT2CHAR", SqlCon);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FLAG", SqlDbType.Int).Value = 1;
        SqlCmd.Parameters.AddWithValue("@code", SqlDbType.VarChar).Value = Code;
        if (SqlCon.State == ConnectionState.Closed)
            SqlCon.Open();
        SqlDataReader DR = SqlCmd.ExecuteReader();
        if (DR.Read())
        {
            string s = DR["serial"].ToString();
            int n = Convert.ToInt32(s);
            string test = Convert.ToString(n);
            Int32 m = n.ToString().Length;
            string sr = DR["format"].ToString();
            Int32 l = sr.Length;
            string sr1 = sr.Substring(l - m);
            string sr2 = sr.Remove(l - m, m);
            string sr3 = sr2.Insert(l - m, test);
            Code = sr3.ToString();
        }
        SqlCon.Close();
        return Code;
    }
    public static string gridPagging(GridView GridView1, DataTable dt)
    {
        string lblcount = "";
        int totalrows = 0;
        GridView1.DataSource = dt;

        GridView1.DataBind();
        try { totalrows = dt.Rows.Count; }
        catch { totalrows = 0; }
        if (totalrows > 0)
        {
            if (GridView1.AllowPaging)
            {
                //if(GridView1.PageIndex>0
                int i = ((GridView1.PageIndex) * GridView1.PageSize) + 1;
                lblcount = i + "-" + (i + GridView1.Rows.Count - 1) + " of " + totalrows;
            }
            else
            {
                lblcount = "All " + totalrows + " records";
            }
        }
        else lblcount = "No Recordes";
        return lblcount;
    }


    //this function Convert to Encode your Password
    public string EncodePasswordToBase64(string password)
    {
        try
        {
            byte[] encData_byte = new byte[password.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
            string encodedData = Convert.ToBase64String(encData_byte);
            return encodedData;
        }
        catch (Exception ex)
        {
            throw new Exception("Error in base64Encode" + ex.Message);
        }
    }
    //this function Convert to Decode your Password
    public string DecodeFrom64(string encodedData)
    {
        try
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encodedData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }
        catch (Exception ex)
        {
            throw new Exception("Error in DecodeFrom64" + ex.Message);
        }

    }
    public object stringFuntion(object str)
    {
        string value = str.ToString().ToLower();
        char[] array = value.ToCharArray();
        // Handle the first letter in the string.
        if (array.Length >= 1)
        {
            if (char.IsLower(array[0]))
            {
                array[0] = char.ToUpper(array[0]);
            }
        }
        for (int i = 1; i < array.Length; i++)
        {
            if (array[i - 1] == ' ')
            {
                if (char.IsLower(array[i]))
                {
                    array[i] = char.ToUpper(array[i]);
                }
            }
        }
        return (new string(array));
    }

    public void SetDDSelectedItemByText(System.Web.UI.WebControls.DropDownList ddl, string txt)
    {
        ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(txt));
    }

}

public class SQLConnectivity
{
    SqlConnection sqlCon;

    public SqlConnection SqlCon
    {
        get { return sqlCon; }
        set { sqlCon = value; }
    }
    SqlCommand sqlCmd;

    public SqlCommand SqlCmd
    {
        get { return sqlCmd; }
        set { sqlCmd = value; }
    }
    SqlDataAdapter sqlAdp;

    public SqlDataAdapter SqlAdp
    {
        get { return sqlAdp; }
        set { sqlAdp = value; }
    }

    public SQLConnectivity()
    {
        sqlCon = new SqlConnection();
        SqlCon.ConnectionString = ConfigurationManager.ConnectionStrings["appConnectionString"].ConnectionString;
    }
}

public class CustomerActivity
{
    public static void CustomerActivityInsert(string ActionDesc,string CustId,string RefId, DateTime Old_NxtshippingDate, DateTime New_NxtshippingDate, int Old_ShippingInterval, int New_ShippingInterval,decimal HappyPointsAmt, decimal CreditGivenAmt)
    {
        PL_Login pobj = new PL_Login();
        pobj.CustId = CustId;
        pobj.ActionDesc = ActionDesc;
        pobj.RefNo = RefId;
        pobj.Old_NxtshippingDate = Old_NxtshippingDate;
        pobj.New_NxtshippingDate = New_NxtshippingDate;
        pobj.Old_ShippingInterval = Old_ShippingInterval;
        pobj.New_ShippingInterval = New_ShippingInterval;
        pobj.HappyPointsAmt = HappyPointsAmt;
        pobj.CreditGivenAmt = CreditGivenAmt;
        BL_Login.InsertCustomerActivity(pobj);
    }
}

