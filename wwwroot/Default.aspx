﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link rel="canonical" href="index.html" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="My account - Chamonix" />
    <meta property="og:image:width" content="1024" />
    <meta property="og:image:height" content="561" />
    <meta name="twitter:card" content="summary_large_image" />
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css?v=1002" rel="stylesheet" />
    <script type="text/javascript" src="/js/sweetalert.min.js"></script>
        <script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body class="bg-gradient-primary">
    <form id="form1" runat="server">
        <div class="container">
            <!-- Outer Row -->
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-9">

                    <div class="card o-hidden border-0 shadow-lg  login-center">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                <div class="col-lg-6">
                                    <div class="p-5 mt-5 mb-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Chamonix Agency Portal</h1>
                                        </div>
                                        <div class="user">
                                            <div class="form-group">
                                                <asp:TextBox runat="server" ID="username" CssClass="form-control form-control-user" placeholder="Login Id"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <asp:TextBox runat="server" ID="password" CssClass="form-control form-control-user" placeholder="Password" TextMode="Password"></asp:TextBox>
                                            </div>
                                            <asp:Button runat="server" ID="btnlogin" CssClass="btn btn-primary btn-user btn-block" Text="Login" OnClick="btnlogin_Click" />
                                            <hr />
                                        </div>
                                       <%-- <div class="text-center">
                                            <asp:LinkButton runat="server" CssClass="small" OnClick="Unnamed_Click">Forgot Password?</asp:LinkButton>
                                        </div>--%>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 center">
                                                <asp:Label ID="lblmessage" CssClass="error" Style="font-size: 13px; font-weight: 500;" runat="server" ForeColor="Red" Visible="false" Text="Unable to proceed please fill correct Email ID and Zipcode!!!"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       

        <script src="/vendor/jquery/jquery.min.js"></script>
        <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="/js/sb-admin-2.min.js"></script>
       

        <script>
            function MPEPaymentRequestDetail() {
                $('#MPEPaymentRequestDetail').modal('show');
            }
            function MPEForgotPassword() {
                $('#MPEForgotPassword').modal('show');
            }
        </script>
    </form>

</body>
</html>
