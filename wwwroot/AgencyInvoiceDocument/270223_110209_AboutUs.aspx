﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>"Priorcric - Best Free Cricket Scoring App | Live Scores & Ball-to-Ball Commentary"</title>
    <meta name="description" content="Get the best free cricket scoring app - Priorcric, featuring live cricket scores today, ball-to-ball cricket commentary, and the latest cricket commentary. Scoreboard app for cricket, online scoring cricket, and cricket leagues near me. Check out BBL PointsTable, Loudoun County Cricket League, Northern California Cricket Association, BBCL Cricket NJ, HCCL Cricket, GPCL Cricket, CPCL Cricket, and NJ Cricket League on Priorcric. Download now and keep track of your favorite cricket leagues and matches!">
    <meta name="keywords" content="Free cricket scoring app, Live cricket scores, Ball-to-ball cricket commentary, Scoreboard app for cricket, Online scoring cricket, Cricket leagues, BBL points table, Big Bash scores, Loudoun County Cricket League, Northern California Cricket Association, BBCL Cricket NJ, HCCL Cricket, GPCL Cricket, CPCL Cricket">


    <!-- Search Engine -->
    <meta name="image" content="https://priorcric.com/images/logo.png">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="Priorcric - Best Free Cricket Scoring App | Live Scores & Ball-to-Ball Commentary">
    <meta itemprop="description" content="Get the best free cricket scoring app - Priorcric, featuring live cricket scores today, ball-to-ball cricket commentary, and the latest cricket commentary. Scoreboard app for cricket, online scoring cricket, and cricket leagues near me. Check out BBL PointsTable, Loudoun County Cricket League, Northern California Cricket Association, BBCL Cricket NJ, HCCL Cricket, GPCL Cricket, CPCL Cricket, and NJ Cricket League on Priorcric. Download now and keep track of your favorite cricket leagues and matches!">
    <meta itemprop="image" content="https://priorcric.com/images/logo.png">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Priorcric - Best Free Cricket Scoring App | Live Scores & Ball-to-Ball Commentary">
    <meta name="twitter:description" content="Get the best free cricket scoring app - Priorcric, featuring live cricket scores today, ball-to-ball cricket commentary, and the latest cricket commentary. Scoreboard app for cricket, online scoring cricket, and cricket leagues near me. Check out BBL PointsTable, Loudoun County Cricket League, Northern California Cricket Association, BBCL Cricket NJ, HCCL Cricket, GPCL Cricket, CPCL Cricket, and NJ Cricket League on Priorcric. Download now and keep track of your favorite cricket leagues and matches!">
    <meta name="twitter:site" content="@PriorwareS">
    <meta name="twitter:creator" content="@PriorwareS">
    <meta name="twitter:image:src" content="https://priorcric.com/images/logo.png">
    <!-- Open Graph general (Facebook & Pinterest) -->
    <meta name="og:title" content="Priorcric - Best Free Cricket Scoring App | Live Scores & Ball-to-Ball Commentary">
    <meta name="og:description" content="Get the best free cricket scoring app - Priorcric, featuring live cricket scores today, ball-to-ball cricket commentary, and the latest cricket commentary. Scoreboard app for cricket, online scoring cricket, and cricket leagues near me. Check out BBL PointsTable, Loudoun County Cricket League, Northern California Cricket Association, BBCL Cricket NJ, HCCL Cricket, GPCL Cricket, CPCL Cricket, and NJ Cricket League on Priorcric. Download now and keep track of your favorite cricket leagues and matches!">
    <meta name="og:image" content="https://priorcric.com/images/logo.png">
    <meta name="og:url" content="https://priorcric.com">
    <meta name="og:site_name" content="Priorware Solutions">
    <meta name="og:type" content="website">
    <!-- Open Graph general (Linkedin) -->
    <meta property='og:title' content="Priorcric - Best Free Cricket Scoring App | Live Scores & Ball-to-Ball Commentary" />
    <meta property='og:description' content="Get the best free cricket scoring app - Priorcric, featuring live cricket scores today, ball-to-ball cricket commentary, and the latest cricket commentary. Scoreboard app for cricket, online scoring cricket, and cricket leagues near me. Check out BBL PointsTable, Loudoun County Cricket League, Northern California Cricket Association, BBCL Cricket NJ, HCCL Cricket, GPCL Cricket, CPCL Cricket, and NJ Cricket League on Priorcric. Download now and keep track of your favorite cricket leagues and matches!" />
    <meta property="og:image" content="https://priorcric.com/images/logo.png" />
    <link rel="canonical" href="https://www.priorcric.com/aboutus" />
    <link rel="alternate" href="https://priorcric.com" hreflang="en-us" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container" style="min-height: 544px; background-color: #fff;">
        <h3 class="title"><span>About Us</span></h3>
        <p style="font-size: 18px;margin: 30px;text-align: justify;">
            Welcome to PriorCric, the home of the best free cricket scoring app! Our app is designed to make scoring and scorekeeping easy and accessible to cricket enthusiasts all around the world. With PriorCric, you can keep track of live cricket scores, ball by ball cricket commentary, and more.

We understand the importance of staying up-to-date with live cricket scores, which is why we offer live cricket scores streaming services. Our app allows you to get the latest cricket scores today, as well as access to live cricket streaming for free. You won't have to miss a moment of the action!

At PriorCric, we are passionate about cricket, which is why we've made it our mission to offer the best free cricket scorekeeping app on the market. Our app is easy to use and comes equipped with all the features you need to keep track of your favorite teams and players.

Our team is dedicated to providing you with the best possible experience, which is why we offer the latest cricket commentary live. With PriorCric, you can stay up-to-date on all the latest news and insights in the world of cricket.

Thank you for choosing PriorCric for all your cricket scorekeeping and streaming needs. We look forward to serving you!
        </p>
    </div>
</asp:Content>

