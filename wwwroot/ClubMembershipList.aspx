﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ClubMembershipList.aspx.cs" Inherits="ClubMembershipList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>


    <style>
        .table-bordered th {
            vertical-align: middle;
        }

            .table-bordered th a {
                color: #ffffff;
            }

        #ContentPlaceHolder1_GridSKUProduct tbody tr {
            white-space: nowrap;
            line-height: 1 !important;
        }

        #ContentPlaceHolder1_GridProduct tbody tr th {
            white-space: nowrap;
            line-height: 1 !important;
            vertical-align: middle !important;
        }

        #ContentPlaceHolder1_GridProduct tbody tr td {
            white-space: nowrap;
            line-height: 1 !important;
            vertical-align: middle !important;
        }

        #ContentPlaceHolder1_GridViewProduct tbody tr {
            white-space: nowrap;
            line-height: 1 !important;
        }

        .product {
            white-space: nowrap;
        }

        .pull-right {
            float: right;
        }

        .gridfloatright {
            text-align: right;
        }

        .form-control {
            border-radius: 5px !important;
            height: 35px !important;
        }

        .displayNone {
            display: none;
        }

        .productcode {
            white-space: nowrap;
        }

        .input-group-addon {
            padding: 9px;
            font-size: 15px;
            font-weight: normal;
            line-height: 1;
            color: #555555;
            text-align: center;
            background-color: #eeeeee;
            border: 1px solid #cccccc;
            border-radius: 0px !important;
        }

        .btnAll {
            height: 35px;
            font-size: 16px;
        }
        /*a {
        color:red !important;
        }*/
        .text-kty, .text-kty th a {
            color: #fff !important;
        }

        tr > th {
            background-color: #36b9cc !important;
        }

        .label, label:not(.variant__button-label) {
            text-transform: none !important;
            letter-spacing: 0 !important;
            font-size: 1rem !important;
        }

        .btn-save {
            text-transform: none !important;
            letter-spacing: 0 !important;
            min-width: 0px !important;
            padding: 0.375rem 0.75rem !important;
        }

        .tablebodytd {
            text-align: center !important;
            vertical-align: middle !important;
        }

        .tablebodytdAmt {
            text-align: right !important;
            vertical-align: middle !important;
        }

        .autocomplete_completionListElement {
            margin-top: -3px !important;
            margin-left: -3px !important;
            background-color: window;
            color: windowtext;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            cursor: 'default';
            overflow: auto;
            text-align: left;
            list-style-type: none;
            max-height: 250px;
            z-index: 99999 !important;
        }
        /* AutoComplete highlighted item */
        .autocomplete_highlightedListItem {
            background-color: #ffff99;
            color: black;
            padding: 1px;
        }
        /* AutoComplete item */
        .autocomplete_listItem {
            background-color: window;
            color: windowtext;
            padding: 1px;
        }

        .btn {
            letter-spacing: 0px !important;
        }
    </style>
    <script type="text/javascript" src="/js/sweetalert.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800 text-center" style="width: 100%">Subscription
            <a href="Home.aspx" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm float-left btn-save">
                <i class="fas fa-arrow-left fa-sm text-white"></i><b style="padding-left: 10px;">Back</b> </a>
        </h1>


    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-2">
            <h6 class="m-0 font-weight-bold text-primary mt-1 mb-1">Subscription Details
                 <div class="float-right">
                     Pending Happy Points:
                     <label style="color: black;" id="lblPendingHP" runat="server"></label>
                     &nbsp;&nbsp;&nbsp;
                 </div>
                <div class="float-right">
                    Available Happy Points:
                     <label style="color: black;" id="lblAvailableHP" runat="server"></label>
                    &nbsp;&nbsp;&nbsp;
                </div>
                <div class="float-right">
                    Total Happy Points:
                     <label style="color: black;" id="lblCMLHP" runat="server"></label>
                    &nbsp;&nbsp;&nbsp;
                </div>
            </h6>
            <h6 class="m-0 font-weight-bold text-primary mt-1 mb-1">
                <label class="col-md-6"></label>
                <div class="col-md-12">
                    <div class="float-right">
                        Pending Credit:
                     <label style="color: black;" id="lblPenidngCredit" runat="server"></label>
                        &nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="float-right">
                        Available Credit:
                     <label style="color: black;" id="lblavailableCredit" runat="server"></label>
                        &nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="float-right">
                        Total Credit:
                     <label style="color: black;" id="lblCMLCredit" runat="server"></label>
                        &nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </h6>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:GridView ID="GridReShipList" runat="server" AutoGenerateColumns="False" AllowSorting="True" class="table table-bordered" OnSorting="GridReShipList_Sorting" OnRowDataBound="GridReShipList_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Signup<br/>Info" SortExpression="SYSorderno" ItemStyle-CssClass="tablebodytd">
                            <ItemTemplate>
                                <asp:Label ID="lblOrderNo" runat="server" Text='<%# Bind("SYSorderno") %>'></asp:Label>
                                <br />
                                <asp:Label ID="lblSourceKey" runat="server" Text='<%# Bind("SourceKey") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>


                        <asp:BoundField HeaderText="Subscribed Product" DataField="Productname" HtmlEncode="false" ItemStyle-Width="25%" ReadOnly="true" ItemStyle-CssClass="product tablebodytd" />

                        <asp:BoundField DataField="Count" HeaderText="Shipment Count" ItemStyle-Width="2%" ReadOnly="true" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                        <asp:TemplateField HeaderText="Last Invoice" SortExpression="lastinvoice" ItemStyle-CssClass="tablebodytd">
                            <ItemTemplate>
                                <asp:Label ID="lbllastinvoice" runat="server" Text='<%# Bind("lastinvoice") %>'></asp:Label>
                                <br />
                                <asp:Label ID="lblrePrevreShipDate" runat="server" Text='<%# Bind("PrevShipdate") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Next<br/>Invoice Date" SortExpression="NextShipDate" ItemStyle-CssClass="tablebodytd">
                            <ItemTemplate>
                                <asp:Label ID="lblreNextreShipDate" runat="server" Text='<%# Bind("NextShipDate") %>'></asp:Label>
                            </ItemTemplate>
                            <%-- <EditItemTemplate>
                                <asp:TextBox ID="txtNxtShipD" Text='<%# Bind("NextShipDate") %>' runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="CCtxtNxtShipD" runat="server" DefaultView="Days" Format="MM/dd/yyyy"
                                    TargetControlID="txtNxtShipD">
                                </asp:CalendarExtender>
                            </EditItemTemplate>--%>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Next<br/>Invoice Amt ($)" ItemStyle-Width="10%" ItemStyle-CssClass="tablebodytd">
                            <ItemTemplate>
                                <asp:Label ID="lblretotalamt" runat="server" Text='<%# Bind("ReshipAmt") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Interval<br/>Days" ItemStyle-CssClass="tablebodytd">
                            <ItemTemplate>
                                <asp:Label ID="LblreShipIntrvl" runat="server" Text='<%# Bind("ShippingInterval") %>'></asp:Label>
                            </ItemTemplate>
                            <%--  <EditItemTemplate>
                                <asp:TextBox ID="txtreShipIntrvl" runat="server" Text='<%# Bind("ShippingInterval") %>' Width="68px"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" TargetControlID="txtreShipIntrvl" FilterType="Numbers" runat="server">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </EditItemTemplate>--%>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Happy Points" SortExpression="ReshippingLogNo" HeaderStyle-ForeColor="#F1F78E" ItemStyle-CssClass="tablebodytd">
                            <ItemTemplate>
                                <asp:Label ID="lblHappyPoint" runat="server" Text='<%#Eval("AHappyPoint") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Credit Given" SortExpression="ReshippingLogNo" HeaderStyle-ForeColor="#F1F78E" ItemStyle-CssClass="tablebodytd">
                            <ItemTemplate>
                                <asp:Label ID="lblCreditGiven" runat="server" Text='<%#Eval("ACreditGiven") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="tablebodytd">
                            <ItemTemplate>
                                <asp:Label ID="lblreStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ReShip No" SortExpression="ReshippingLogNo" HeaderStyle-ForeColor="#F1F78E" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lnkReShipLogNo" runat="server" Text='<%#Eval("ReshippingLogNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="StateTax" SortExpression="ReshippingLogNo" HeaderStyle-ForeColor="#F1F78E" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblStateTax" runat="server" Text='<%#Eval("StateTax") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NoOfProduct" SortExpression="ReshippingLogNo" HeaderStyle-ForeColor="#F1F78E" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lnkNoOfProduct" runat="server" Text='<%#Eval("NoOfProduct") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--  <asp:BoundField DataField="NoOfProduct" HeaderText="NoOfProduct" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />--%>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnChangeNextShipDate" runat="server" ForeColor="#157ab5" Text='Change Next Ship Date' OnClick="lbtnChangeNextShipDate_Click"></asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="lblbtnChangeInterval" runat="server" ForeColor="#157ab5" Text='Change Order Frequency' OnClick="lblbtnChangeInterval_Click" Style="white-space: nowrap;"></asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="lnkCancelSubscription" runat="server" ForeColor="#157ab5" Text='Cancel Subscription' OnClick="lnkCancelSubscription_Click"></asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="lnkCancelProduct" runat="server" ForeColor="#157ab5" Text='Add Product' OnClick="lnkCancelProduct_Click"></asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="lnkRemoveProduct" runat="server" ForeColor="#157ab5" Text='Remove Product' OnClick="lnkRemoveProduct_Click"></asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="lnkShippingAddress" runat="server" ForeColor="#157ab5" Text='Add Shipping Address' OnClick="lnkShippingAddress_Click"></asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="lnkHappyPoint" runat="server" ForeColor="#157ab5" Text='Apply Happy Points' OnClick="lnkHappyPoint_Click"></asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="lnkCreditGiven" runat="server" ForeColor="#157ab5" Text='Apply Company Credit' OnClick="lnkCreditGiven_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle CssClass="text-kty bg-info text-center align-middle" />
                </asp:GridView>

            </div>
        </div>
    </div>

    <div id="MPERevertCompanyCredit" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change Next Ship Date</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Last Invoice Date
                                </label>
                                <asp:HiddenField runat="server" ID="hdnReshippLogNo" />
                                <asp:HiddenField runat="server" ID="hdnStatus" />
                                <asp:HiddenField runat="server" ID="hdnInterval" />
                                <asp:HiddenField runat="server" ID="hdnNextShipDate" />
                                <asp:HiddenField runat="server" ID="hdnStateTax" />
                                <asp:HiddenField runat="server" ID="hdnPreInvoiceDate" />
                                <asp:TextBox ID="txtlastInvoiceDate" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Next Ship Date
                                </label>
                                <asp:TextBox ID="txtNextShipDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:CalendarExtender ID="CCtxtNxtShipD" runat="server" DefaultView="Days" Format="MM/dd/yyyy"
                                    TargetControlID="txtNextShipDate">
                                </asp:CalendarExtender>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnUpdateNextShipDate" runat="server" CssClass="btn btn-success btn-sm pull-right btn-save"
                                Text="Update" OnClick="btnUpdateNextShipDate_Click" />
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <%--  <div class="submit-section">
                        <asp:Button ID="Button5" runat="server" CssClass="btn btn-primary btn-sm"
                            Text="Revert Company Credit" OnClick="btnRevertCompanyCredit_Click" ValidationGroup="RevertCompanyCredit" />
                        <asp:Button ID="btnRevertCompanyCreditclosed" runat="server" Text="Close"
                            CssClass="btn btn-primary btn-sm" />
                    </div>--%>
                </div>
            </div>
        </div>
    </div>

    <div id="MPEChangeInterval" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change Order Frequency</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Interval(Days)
                                </label>
                                <asp:TextBox ID="txtPreInterval" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Change Interval(Days)
                                </label>
                                <asp:TextBox ID="txtChangeInterval" runat="server" CssClass="form-control"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" TargetControlID="txtChangeInterval"
                                    FilterType="Numbers" runat="server">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnUpdateInterval" runat="server" CssClass="btn btn-success btn-sm pull-right btn-save"
                                Text="Update" OnClick="btnUpdateInterval_Click" />
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <%--  <div class="submit-section">
                        <asp:Button ID="Button5" runat="server" CssClass="btn btn-primary btn-sm"
                            Text="Revert Company Credit" OnClick="btnRevertCompanyCredit_Click" ValidationGroup="RevertCompanyCredit" />
                        <asp:Button ID="btnRevertCompanyCreditclosed" runat="server" Text="Close"
                            CssClass="btn btn-primary btn-sm" />
                    </div>--%>
                </div>
            </div>
        </div>
    </div>

    <div id="MPECancelOrder" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Cancel Subscription Request</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center; margin-top: 5px; color: red;">
                        <h5>A Customer service representative will be contacting you as soon as possible! 
                            <br />
                            Monday-Friday 9am-6pm EST.</h5>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Name
                                </label>
                                <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hdnSysOrderNo" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Email Id
                                </label>
                                <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Phone Number
                                </label>
                                <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Subject
                                </label>
                                <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    Reason
                                </label>
                                <asp:TextBox ID="txtReason" TextMode="MultiLine" Width="100%" runat="server" CssClass="form-control" Style="height: 80px !important"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-success btn-sm pull-right btn-save"
                                Text="Save" OnClick="BtnSave_Click" />
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <%--  <div class="submit-section">
                        <asp:Button ID="Button5" runat="server" CssClass="btn btn-primary btn-sm"
                            Text="Revert Company Credit" OnClick="btnRevertCompanyCredit_Click" ValidationGroup="RevertCompanyCredit" />
                        <asp:Button ID="btnRevertCompanyCreditclosed" runat="server" Text="Close"
                            CssClass="btn btn-primary btn-sm" />
                    </div>--%>
                </div>
            </div>
        </div>
    </div>

    <div id="MPEChangeProduct" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <asp:TextBox ID="txtProductName" runat="server" CssClass="form-control" placeholder="Enter Product Name" Style="height: 35px;"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdnNoOfProduct" />
                        </div>
                        <%--<div class="col-md-4">
                            <asp:TextBox ID="txtProductId" runat="server" placeholder="Enter Product Code" CssClass="form-control"></asp:TextBox>
                        </div>--%>
                        <div class="col-md-1">
                            <asp:Button ID="btnProductSearch" runat="server" Text="Search" CssClass="btn btn-primary btn-sm btnAll btn-save" OnClick="btnProductSearch_Click" />
                        </div>
                        <div class="col-md-2">
                            <asp:Button ID="btnAddProduct" runat="server" Text="Add Product" CssClass="btn btn-success btn-sm btnAll btn-save" OnClientClick="AddProducthide()" OnClick="btnAddProduct_Click" />
                        </div>
                    </div>
                    <br />
                    <div class="row" style="height: 250px; width: 100%; overflow: auto">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <asp:DataPagerGridView ID="GridSKUProduct" runat="server" CssClass="table table-bordered"
                                    AutoGenerateColumns="False" Width="100%">
                                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkProduct" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Product ID" ItemStyle-Width="8%" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProductID2" runat="server" Text='<%# Bind("ProductID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ProdCode" HeaderText="Item" ReadOnly="True" HeaderStyle-CssClass="productcode" ItemStyle-CssClass="productcode" FooterStyle-CssClass="productcode" />
                                        <asp:BoundField DataField="ProductName" HeaderText="Description" ReadOnly="True" />
                                        <asp:TemplateField HeaderText="Regular Price($)" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPunitPrice" runat="server" Text='<%# Bind("PunitPrice") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="gridfloatright" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Your Price($)" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblYourPrice" runat="server" Text='<%# Bind("YourPrice") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="gridfloatright" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty" ItemStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPQty" runat="server" Text='<%# Bind("PQty") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="text-kty bg-info text-center align-middle" />
                                </asp:DataPagerGridView>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div runat="server" id="divproduct" visible="false">
                        <h4>Your New Package</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <asp:DataPagerGridView ID="GridProduct" runat="server" CssClass="table table-bordered"
                                        AutoGenerateColumns="False" Width="100%">
                                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Product ID" ItemStyle-Width="8%" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMainProductID" runat="server" Text='<%# Bind("ProductID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ProdCode" HeaderText="Item" ReadOnly="True" HeaderStyle-CssClass="productcode" ItemStyle-CssClass="productcode" FooterStyle-CssClass="productcode" />
                                            <asp:BoundField DataField="ProductName" HeaderText="Description" ReadOnly="True" />
                                            <asp:TemplateField HeaderText="Regular Price($)" ItemStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMainPunitPrice" runat="server" Text='<%# Bind("PunitPrice") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="gridfloatright" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Your Price($)" ItemStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMainYourPrice" runat="server" Text='<%# Bind("YourPrice") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="gridfloatright" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Qty" ItemStyle-Width="18%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" ID="imgminus" ImageUrl="~/img/minus.png" Height="25px" Width="25px" Style="vertical-align: sub; border: 0 !important; padding: 0px !important" OnClick="imgminus_Click" />
                                                    <asp:TextBox ID="txtMainQty" runat="server" Text='<%# Bind("PQty") %>' Width="60px" Style="text-align: center;" Enabled="false"></asp:TextBox>
                                                    <asp:ImageButton runat="server" ID="imgplus" ImageUrl="~/img/plus.png" Height="25px" Width="25px" Style="vertical-align: sub; border: 0 !important; padding: 0px !important" OnClick="imgplus_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="text-kty bg-info text-center align-middle" />
                                    </asp:DataPagerGridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Button ID="btnConfirmmsg" runat="server" CssClass="btn btn-success btn-sm btnAll pull-right btn-save"
                                    Text="Save" OnClick="btnConfirmmsg_Click" />
                                <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-success btn-sm btnAll pull-right btn-save"
                                    Text="Save" OnClick="btnAdd_Click" Style="display: none;" />
                                <%--<asp:Button ID="btnNext" runat="server" CssClass="btn btn-success btn-sm btnAll pull-right"
                                    Text="Next" OnClick="btnNext_Click" />--%>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <%--  <div class="submit-section">
                        <asp:Button ID="Button5" runat="server" CssClass="btn btn-primary btn-sm"
                            Text="Revert Company Credit" OnClick="btnRevertCompanyCredit_Click" ValidationGroup="RevertCompanyCredit" />
                        <asp:Button ID="btnRevertCompanyCreditclosed" runat="server" Text="Close"
                            CssClass="btn btn-primary btn-sm" />
                    </div>--%>
                </div>
            </div>
        </div>
    </div>

    <div id="MPEViewProduct" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">View Your Package</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <asp:DataPagerGridView ID="GridViewProduct" runat="server" CssClass="table table-bordered"
                                    AutoGenerateColumns="False" Width="100%" OnRowDataBound="GridViewProduct_RowDataBound">
                                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                    <Columns>
                                        <asp:BoundField DataField="ProdCode" HeaderText="Item" ReadOnly="True" HeaderStyle-CssClass="productcode" ItemStyle-CssClass="productcode" FooterStyle-CssClass="productcode" />
                                        <asp:BoundField DataField="ProductName" HeaderText="Description" ReadOnly="True" />
                                        <asp:TemplateField HeaderText="Your Price($)" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblViewYourPrice" runat="server" Text='<%# Bind("YourPrice") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="gridfloatright" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty" ItemStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblViewPQty" runat="server" Text='<%# Bind("PQty") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblViewTotal" runat="server" Text='0'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="gridfloatright" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="text-kty bg-info text-center align-middle" />
                                </asp:DataPagerGridView>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row form-group">
                        <div class="col-md-2">
                            <label>
                                Next Ship Date
                            </label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtNextShipDateView" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <label>
                                Sub Total
                            </label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtSubTotal" runat="server" CssClass="form-control" Enabled="false" Style="text-align: right"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-2">
                            <label>
                                Interval (Days)
                            </label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtInterval" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <label>
                                State Sales Tax
                            </label>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group" style="float: left; width: 49%">
                                <span class="input-group-addon">%</span>
                                <asp:TextBox ID="txtTax" runat="server" CssClass="form-control" Enabled="false" Style="width: 70px; text-align: right; border-radius: 0px;"></asp:TextBox>
                            </div>
                            <div class="input-group" style="float: left; width: 51%">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txtamt" runat="server" CssClass="form-control" Enabled="false" Style="width: 70px; text-align: right; border-radius: 0px;"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-2">
                            <label>
                                Total
                            </label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control" Enabled="false" Style="text-align: right"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div id="MPERemoveProduct" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Remove Subscribed Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-2">
                            <label>
                                Order No
                            </label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtOrderNo" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdnAutoId" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <asp:DataPagerGridView ID="grdRemoveProduct" runat="server" CssClass="table table-bordered"
                                    AutoGenerateColumns="False" Width="100%">
                                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                    <Columns>
                                        <asp:BoundField DataField="AutoId" HeaderText="AutoId" ReadOnly="True" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                        <asp:BoundField DataField="ProductName" HeaderText="Subscribed Product" ReadOnly="True" />
                                        <asp:TemplateField HeaderText="Remove">
                                            <ItemTemplate>
                                                <center>
                                                    <asp:ImageButton ID="ImgDeleteTransaction" runat="server" Text="Delete" CommandArgument='<%# Bind("AutoId") %>'
                                                        OnClick="ImgDeleteTransaction_Click" ToolTip="Delete" ImageUrl="~/ScriptsAndCSS/CSS/Images/delete.png" Style="height: 19px; width: 19px; border: 0!important; padding: 0px !important">
                                                    </asp:ImageButton>
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="text-kty bg-info text-center align-middle" />
                                </asp:DataPagerGridView>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnRemoveProduct" runat="server" CssClass="btn btn-success btn-sm btnAll pull-right btn-save"
                        Text="Next" OnClick="btnRemoveProduct_Click" Style="display: none;" />
                </div>
            </div>
        </div>
    </div>


    <div id="MPEAddHP" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Apply Happy Points</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Next Invoice Amt($)
                                </label>
                                <asp:TextBox ID="txtHPNextInvoiceAmt" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    New Next Invoice Amt($)
                                </label>
                                <asp:TextBox ID="txtHPNewNextInvoiceAmt" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Available Happy Points
                                </label>
                                <asp:TextBox ID="txtAvailableHP" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Apply Happy Points
                                </label>
                                <asp:DropDownList ID="ddlRedemOption" runat="server" CssClass="custom-select custom-select-sm form-control form-control-sm " AutoPostBack="true" OnSelectedIndexChanged="ddlRedemOption_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtHappyPoints" runat="server" CssClass="form-control" Text="0" Style="display: none"></asp:TextBox>
                                 <asp:TextBox ID="txtHappyMoney" runat="server" CssClass="form-control" Text="0" Style="display: none"></asp:TextBox>
                                <asp:TextBox ID="txtCouponAmt" runat="server" Style="text-align: right;display:none;"
                                    CssClass="form-control form-control-sm" Enabled="False" Text="0.00"></asp:TextBox>
                                 <asp:TextBox ID="txtRedeemCoupanCode" runat="server" CssClass="form-control form-control-sm" Enabled="false" Style="display: none"></asp:TextBox>
                                 <asp:HiddenField runat="server" ID="hdnRedeemPoints" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnUpdateHP" runat="server" CssClass="btn btn-success btn-sm pull-right btn-save"
                                Text="Update" OnClick="btnUpdateHP_Click" />
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div id="MPEAddCreditGiven" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Apply Company Credit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Next Invoice Amt($)
                                </label>
                                <asp:TextBox ID="txtCG_NextInvoiceAmt" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    New Next Invoice Amt($)
                                </label>
                                <asp:TextBox ID="txtCG_New_NextInvoiceAmt" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Available Credit
                                </label>
                                <asp:TextBox ID="txtAvailableCredit" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Apply Company Credit
                                </label>
                                <asp:TextBox ID="txtaddcredit" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtaddcredit"
                                    FilterType="Numbers" runat="server">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnUpdateCredit" runat="server" CssClass="btn btn-success btn-sm pull-right btn-save"
                                Text="Update" OnClick="btnUpdateCredit_Click" />
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <%----------------------------------UPDATE SHIPPING INFORMATION--------------------------%>
    <div id="MPEShipAddress" class="modal custom-modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title">Shipping Address List</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive" style="height: 350px;">
                                        <asp:GridView ID="GridShipPop" runat="server" GridLines="None" BorderWidth="0" CssClass="table table-bordered" EmptyDataText="No Record Found"
                                            AutoGenerateColumns="False">
                                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemTemplate>
                                                        <center>
                                                            <asp:RadioButton ID="chkdefault1" runat="server" Checked='<%#Eval("Is Default") %>'
                                                                OnClick="SelectSingleRadiobutton(this.id)" />
                                                        </center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="STFirstName" HeaderText="First Name" SortExpression="STFirstName" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                                <asp:BoundField DataField="STLastName" HeaderText="Last Name" SortExpression="STLastName" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                                <asp:TemplateField HeaderText="Address">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAddress1" runat="server" Text='<%#Eval("Fulladdress") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="STAddress" HeaderText="Address1" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                                <asp:BoundField DataField="STAddress1" HeaderText="Address2" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                                <asp:BoundField DataField="STCity" HeaderText="City" SortExpression="STCity" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                                <asp:BoundField DataField="STState" HeaderText="State" SortExpression="STState" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                                <asp:BoundField DataField="STZip" HeaderText="ZipCode" SortExpression="STZip" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                                <asp:BoundField DataField="STCountry" HeaderText="Country" SortExpression="STCountry" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                                                <asp:BoundField DataField="STPhoneNo" HeaderText="Home Phone" />
                                                <asp:BoundField DataField="STPhoneNo1" HeaderText="Mobile" />
                                                <asp:BoundField DataField="SofficeNo" HeaderText="Office Phone" HeaderStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" />
                                                <asp:BoundField DataField="STEmailID" HeaderText="Email Id" />
                                                <asp:TemplateField HeaderText="Edit" SortExpression="ShippingEntryID" HeaderStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="LnkShipId" runat="server" Text="Edit" CommandArgument='<%# Bind("ShippingEntryID") %>'
                                                            ToolTip="Edit" ImageUrl="~/ScriptsAndCSS/CSS/Images/edit2.png" Style="height: 19px; width: 19px;"></asp:ImageButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="text-kty bg-info text-center align-middle" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="submit-section">
                                <asp:Button ID="btnsaveShipping" runat="server" Text="Update shipping address for all orders" CssClass="btn btn-success btn-sm"
                                    OnClick="btnsaveShipping_OnClick" />
                                &nbsp; &nbsp;<asp:Button ID="btnShipUsethis" runat="server" Text="Update shipping address for current order"
                                    CssClass="btn btn-success btn-sm" OnClick="btnShipUsethis_Click" />
                                &nbsp; &nbsp;<asp:Button ID="btn_addnewShipng" runat="server" Text="Add New" CssClass="btn btn-primary btn-sm"
                                    OnClick="btn_addnewShipng_Click" />
                                &nbsp; &nbsp;<asp:Button ID="btnclosepopshipping" runat="server" Text="Close" CssClass="btn btn-primary btn-sm" OnClick="btnclosepopshipping_Click" Style="display: none" />
                            </div>
                        </div>
                    </ContentTemplate>


                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnsaveShipping" />
                    </Triggers>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnclosepopshipping" />
                    </Triggers>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btn_addnewShipng" />
                    </Triggers>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnShipUsethis" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

        </div>
    </div>


    <div id="MPENewShippingAddres" class="modal custom-modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title">Add Shipping Address</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            First Name<span class="text-danger">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="Fill First Name"
                                                Text="*" ControlToValidate="txtS_Fname" ValidationGroup="IDSnewS" Display="None"></asp:RequiredFieldValidator>
                                            <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender49" runat="server" TargetControlID="RequiredFieldValidator29"
                                                PopupPosition="BottomLeft">
                                            </asp:ValidatorCalloutExtender>
                                        </label>
                                        <asp:TextBox ID="txtS_Fname" runat="server" CssClass="form-control form-control-sm" Style="text-transform: capitalize;"></asp:TextBox>
                                        <asp:HiddenField runat="server" ID="hdnShippingId" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            Last Name<span class="text-danger">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator181" runat="server" ErrorMessage="Fill Last Name"
                                                Text="*" ControlToValidate="txtS_Lname" ValidationGroup="IDSnewS" Display="None"></asp:RequiredFieldValidator>
                                            <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender181" runat="server" TargetControlID="RequiredFieldValidator181"
                                                PopupPosition="BottomLeft">
                                            </asp:ValidatorCalloutExtender>
                                        </label>
                                        <asp:TextBox ID="txtS_Lname" runat="server" CssClass="form-control form-control-sm" Style="text-transform: capitalize;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            Address 1<span class="text-danger">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="Fill Address"
                                                Text="*" ControlToValidate="txtnewS_add1" ValidationGroup="IDSnewS" Display="None"></asp:RequiredFieldValidator>
                                            <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender50" runat="server" TargetControlID="RequiredFieldValidator31"
                                                PopupPosition="BottomLeft">
                                            </asp:ValidatorCalloutExtender>
                                        </label>
                                        <asp:TextBox ID="txtnewS_add1" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="display: none">
                                    <div class="form-group">
                                        <label>
                                            Company
                                        </label>
                                        <asp:TextBox ID="txtnewS_Company" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            Address 2
                                        </label>
                                        <asp:TextBox ID="txtnewS_add2" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            Zip Code<span class="text-danger">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="Fill Zip Code"
                                                Text="*" ControlToValidate="txtnewS_zip" ValidationGroup="IDSnewS" Display="None"></asp:RequiredFieldValidator>
                                            <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender53" runat="server" TargetControlID="RequiredFieldValidator34"
                                                PopupPosition="BottomLeft">
                                            </asp:ValidatorCalloutExtender>
                                        </label>
                                        <asp:TextBox ID="txtnewS_zip" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True"
                                            OnTextChanged="txtnewS_zip_TextChanged" MaxLength="6"></asp:TextBox>
                                        <asp:AutoCompleteExtender runat="server" BehaviorID="AutoComplete3" ID="AutoCompleteExtender4"
                                            TargetControlID="txtnewS_zip" ServicePath="~/MM/AutoComplete.asmx" ServiceMethod="GetZipCodeCompletionList"
                                            MinimumPrefixLength="1" CompletionInterval="100" CompletionListCssClass="autocomplete_completionListElement"
                                            CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                            DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="True" Enabled="True">
                                        </asp:AutoCompleteExtender>

                                        <asp:HiddenField ID="hdnIsTxtnewS_zip_TextChanged" runat="server" Value="0" />
                                        <asp:HiddenField ID="hdnIsTxtnewB_zipcode_TextChanged" runat="server" Value="0" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            Country
                                        </label>
                                        <asp:DropDownList ID="ddlnewS_country" runat="server" CssClass="form-control  form-control-sm" TabIndex="35"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlnewS_country_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            State<span class="text-danger">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator57" runat="server" ErrorMessage="Fill State"
                                                Text="*" ControlToValidate="ddlnewS_state" ValidationGroup="IDSnewS" Display="None"
                                                InitialValue="Select State"></asp:RequiredFieldValidator>
                                            <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender61" runat="server" TargetControlID="RequiredFieldValidator57"
                                                PopupPosition="BottomLeft">
                                            </asp:ValidatorCalloutExtender>
                                        </label>
                                        <asp:DropDownList ID="ddlnewS_state" runat="server" CssClass="form-control  form-control-sm" TabIndex="34"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlnewS_state_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            City<span class="text-danger">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator58" runat="server" ErrorMessage="Fill City"
                                                Text="*" ControlToValidate="ddlnewS_city" ValidationGroup="IDSnewS" Display="None"
                                                InitialValue="Select City"></asp:RequiredFieldValidator>
                                            <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender62" runat="server" TargetControlID="RequiredFieldValidator58"
                                                PopupPosition="BottomLeft">
                                            </asp:ValidatorCalloutExtender>
                                        </label>
                                        <asp:DropDownList ID="ddlnewS_city" runat="server" CssClass="form-control form-control-sm" TabIndex="33"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlnewS_city_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            Home&nbsp;
                                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/img/phone.png" />
                                        </label>
                                        <asp:TextBox ID="txtnewS_phno" runat="server" CssClass="form-control form-control-sm" MaxLength="11"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender51" FilterType="Numbers"
                                            runat="server" TargetControlID="txtnewS_phno">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="row">


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            Mobile&nbsp;
                                                    <asp:Image ID="Image6" runat="server" ImageUrl="~/img/mobile.png" />
                                        </label>
                                        <asp:TextBox ID="txtnewS_Phone2" runat="server" CssClass="form-control form-control-sm" MaxLength="11"
                                            TabIndex="34"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterType="Numbers"
                                            Enabled="True" TargetControlID="txtnewS_Phone2">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-md-4" style="display: none">
                                    <div class="form-group">
                                        <label>
                                            Office Phone
                                        </label>
                                        <asp:TextBox ID="txtnewS_OfficeNo" runat="server" CssClass="form-control form-control-sm" MaxLength="11"
                                            TabIndex="34"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                            Enabled="True" TargetControlID="txtnewS_OfficeNo">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            Email ID
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="None"
                                            ErrorMessage="Fill Valid Email Id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ViewStateMode="Disabled" ControlToValidate="txtnewS_email"></asp:RegularExpressionValidator>
                                            <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender54" runat="server" TargetControlID="RegularExpressionValidator4"
                                                PopupPosition="BottomLeft">
                                            </asp:ValidatorCalloutExtender>
                                        </label>
                                        <asp:TextBox ID="txtnewS_email" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">


                                    <asp:Button ID="btnnewshippingA" runat="server" CssClass="btn btn-success btn-sm"
                                        Text="Save shipping address for all orders" OnClick="btnnewshippingA_Click" ValidationGroup="IDSnewS" />
                                    &nbsp; &nbsp;
                                            <asp:Button ID="btnnewshippingUseThisAdd" runat="server" CssClass="btn btn-success btn-sm"
                                                Text="Save shipping address for current order" OnClick="btnnewshippingUseThisAdd_Click" ValidationGroup="IDSnewS" />
                                    &nbsp; &nbsp;<asp:Button ID="btnclosepopnewshipping" runat="server" Text="Close" CssClass="btn btn-primary btn-sm" OnClick="btnclosepopnewshipping_Click" Style="display: none" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="text-align: center; font-size: small; font-style: italic">
                                    <asp:Label runat="server" Font-Bold="true" ID="lblmsg2" Visible="false"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </ContentTemplate>

                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnnewshippingA" />
                    </Triggers>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnclosepopnewshipping" />
                    </Triggers>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnnewshippingUseThisAdd" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <div id="MPEPaymentRequestDetail" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-size: large !important; text-transform: inherit !important; letter-spacing: 0 !important; color: blueviolet !important;">
                                If you are experiencing any issues please contact us at cs@chamonixstore.com or call 1-800-754-6211 Monday - Friday 9am- 5pm EST. 
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnClose" runat="server" CssClass="btn btn-success btn-sm"
                        Text="Close" />
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function PopupOpenMPERevertCompanyCredit() {
            $('#MPERevertCompanyCredit').modal('show');
        }
        function PopupOpenMPECancelOrder() {
            $('#MPECancelOrder').modal('show');
        }
        function PopupOpenMPEChangeProduct() {
            $('#MPEChangeProduct').modal('show');
        }
        function PopupOpenMPEGridViewProduct() {
            $('#MPEViewProduct').modal('show');
        }
        function PopupOpenMPEMPEChangeInterval() {
            $('#MPEChangeInterval').modal('show');
        }
        function AddProducthide() {
            $("#ContentPlaceHolder1_btnAddProduct").hide();
        }
        function PopupOpenMPERemoveProduct() {
            $('#MPERemoveProduct').modal('show');
        }
        function PopupOpenMPEMPEAddHP() {
            $('#MPEAddHP').modal('show');
        }
        function PopupOpenMPEAddCreditGiven() {
            $('#MPEAddCreditGiven').modal('show');
        }
        function PopupOpenMPEShipAddress() {
            $('#MPEShipAddress').modal('show');
        }
        function PopupOpenMPEShipAddressHide() {
            $('#MPEShipAddress').modal('hide');
        }
        function PopupOpenMPENewShippingAddres() {
            $('#MPENewShippingAddres').modal('show');
        }
        function MPEPaymentRequestDetail() {
            $('#MPEPaymentRequestDetail').modal('show');
        }
        function MPEShipAddressPopupClosedBtnClick() {
            document.getElementById('<%= btnclosepopshipping.ClientID %>').click();
        }
        function MPENewShipAddressPopupClosedBtnClick() {
            document.getElementById('<%= btnclosepopnewshipping.ClientID %>').click();
            $(".show").removeClass();
        }
        function MPEConfirmmationmsgChangeProduct() {
            swal({
                title: "Are you sure?",
                text: "Do you want to change product?",
                icon: "warning",
                dangerMode: true,
                buttons: true,
                buttons: ['No', 'Yes']
            }).then(willDelete => {
                if (willDelete) {
                    document.getElementById('ContentPlaceHolder1_btnAdd').click();
                }
                else {

                }
            });
        }
        function MPEConfirmmationmsgRemoveProduct() {
            swal({
                title: "Are you sure?",
                text: "Do you want to remove product?",
                icon: "warning",
                dangerMode: true,
                buttons: true,
                buttons: ['No', 'Yes']
            }).then(willDelete => {
                if (willDelete) {
                    document.getElementById('ContentPlaceHolder1_btnRemoveProduct').click();
                }
                else {

                }
            });
        }

        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }

    </script>

</asp:Content>

