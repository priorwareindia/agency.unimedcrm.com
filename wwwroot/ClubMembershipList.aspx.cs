﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Web.Services;
using DLLInvoiceDetail;
using DLLCustomerDetail;
using System.Web.Script.Serialization;

public partial class ClubMembershipList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Session_UserloginID"] != null)
            {
                ViewState.Add("Who", Session["Session_UserloginID"].ToString());
                bindform();
                BindPendingHappyPoints();
                BindPendingCredit();
                bindgridReship();
                bindddlnewB_country(ddlnewS_country);
                FetchRedeemOptionYotpo();
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEGridViewProduct1", "PopupOpenMPEGridViewProduct();", true);
            }
            else
            {
                Response.Redirect("~/Default.aspx", false);
            }

        }
    }

    protected void GridReShipList_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortingDirection = string.Empty;
            if (dir == SortDirection.Ascending)
            {
                dir = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                dir = SortDirection.Ascending;
                sortingDirection = "Asc";
            }
            DataView sortedView = new DataView(sortbindgrid8());
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            GridReShipList.DataSource = sortedView;
            GridReShipList.DataBind();
        }
        catch (Exception ex)
        { }
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected DataTable sortbindgrid8()
    {
        DataTable dt = ViewState["Reshipdt"] as DataTable;
        return dt;
    }
    private void bindgridReship()
    {
        try
        {
            PL_InvoiceDetail pobjCo = new PL_InvoiceDetail();
            {
                pobjCo.CustID = Session["Session_UserloginID"].ToString();
            }
            BL_InvoiceDetail.SearchContInv(pobjCo);
            ViewState.Add("Reshipdt", pobjCo.Ds.Tables[0]);
            bindgridUsingViewState();
        }
        catch { }
    }
    private void bindgridUsingViewState()
    {
        try
        {
            GridReShipList.DataSource = ViewState["Reshipdt"];
            GridReShipList.DataBind();
        }
        catch { }
    }

    protected void GridReShipList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            string status = "";
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkHappyPoint = e.Row.FindControl("lnkHappyPoint") as LinkButton;
                LinkButton lnkCreditGiven = e.Row.FindControl("lnkCreditGiven") as LinkButton;
                e.Row.Cells[1].Text = e.Row.Cells[1].Text.Replace(",", "</br>").Trim();
                //((Label)e.Row.FindControl("lblLastDeclineDate3")).Text = ((Label)e.Row.FindControl("lblLastDeclineDate3")).Text.Replace(" ", "<br/>").Replace("<br/>AM", " AM").Replace("<br/>PM", " PM");
                decimal AmtPerHappyPoint = 0;
                PL_InvoiceDetail pobjh = new PL_InvoiceDetail();
                BL_InvoiceDetail.BindHappyPoints(pobjh);
                if (pobjh.Ds.Tables[0].Rows.Count > 0)
                {
                    AmtPerHappyPoint = Convert.ToDecimal(pobjh.Ds.Tables[0].Rows[0]["AmntPerHappyPoints"].ToString());
                }
                decimal HP = 0;
                decimal Credit = 0;
                try
                {
                    HP = Convert.ToInt32(lblCMLHP.InnerText);
                }
                catch
                {
                    HP = 0;
                }
                if (HP < AmtPerHappyPoint)
                {
                    lnkHappyPoint.Visible = false;
                }
                else
                {
                    lnkHappyPoint.Visible = true;
                }
                try
                {
                    Credit = Convert.ToInt32(lblCMLCredit.InnerText);
                }
                catch
                {
                    Credit = 0;
                }
                if (Credit < 1)
                {
                    lnkCreditGiven.Visible = false;
                }
                else
                {
                    lnkCreditGiven.Visible = true;
                }
            }
        }
        catch { }
    }

    protected void lbtnChangeNextShipDate_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            hdnReshippLogNo.Value = (((Label)gr.FindControl("lnkReShipLogNo")).Text);
            hdnInterval.Value = (((Label)gr.FindControl("LblreShipIntrvl")).Text);
            hdnStatus.Value = ((Label)gr.FindControl("lblreStatus")).Text.Trim();
            txtlastInvoiceDate.Text = (((Label)gr.FindControl("lblrePrevreShipDate")).Text);
            txtNextShipDate.Text = (((Label)gr.FindControl("lblreNextreShipDate")).Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPERevertCompanyCredit", "PopupOpenMPERevertCompanyCredit();", true);
        }
        catch
        {

        }
    }

    protected void btnUpdateNextShipDate_Click(object sender, EventArgs e)
    {
        try
        {
            PropertyLayerConitnousOrder pobj = new PropertyLayerConitnousOrder();

            DateTime PrevDate = (Convert.ToDateTime(txtlastInvoiceDate.Text));
            DateTime ChangeDate = (Convert.ToDateTime(txtNextShipDate.Text));
            if ((ChangeDate.Date - PrevDate.Date).Days > 180)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Next Shipment Date can change always less than 180 Days', 'error')", true);
                return;
            }

            if (Convert.ToDateTime(txtNextShipDate.Text) < DateTime.Now)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Next Shipment Date always greater than current date.', 'error')", true);
                return;
            }

            pobj.ReShippingNo = Int32.Parse(hdnReshippLogNo.Value);
            pobj.ShippingInterval = Convert.ToInt32(hdnInterval.Value);
            if (pobj.ShippingInterval != 0)
            {
                pobj.NxtshippingDate = (Convert.ToDateTime(txtNextShipDate.Text));
                pobj.Status = hdnStatus.Value;
                pobj.OrderType = "";
                pobj.Who = ViewState["Who"].ToString();
                BusinessContinousOrder.updateReshipIntr(pobj);
                if (!pobj.IsException)
                {
                    CustomerActivity.CustomerActivityInsert("Customer has changed Next Ship Date.", pobj.Who, pobj.ReShippingNo.ToString(), PrevDate, ChangeDate, 0, 0, 0, 0);
                    bindgridReship();
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Next Ship Date changed successfully.', 'success')", true);

                }
            }
            else

            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Shipping Interval is zero so please enter non zero value.', 'error')", true);
            }

        }
        catch (Exception ex)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Server Busy , Try After Some Time', 'error')", true);
        }
    }

    protected void lnkCancelSubscription_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            hdnSysOrderNo.Value = (((Label)gr.FindControl("lblOrderNo")).Text);
            txtSubject.Text = "Order " + hdnSysOrderNo.Value + "- Call Back-Request to cancel.";
            bindform();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPECancelOrder", "PopupOpenMPECancelOrder();", true);
        }
        catch
        {

        }
    }

    private void bindform()
    {
        PL_CustomerDetail pobj = new PL_CustomerDetail();
        if (Session["Session_UserloginID"] != null)
        {
            pobj.CustID = Session["Session_UserloginID"].ToString();

            #region Customerdeatials
            BL_CustomerDetail.selectCustomerdetails(pobj);
            if (!pobj.IsException)
            {
                if (pobj.Ds.Tables[0].Rows.Count > 0)
                {
                    //try
                    //{
                    //    lblCMLHP.InnerText = Convert.ToInt32(pobj.Ds.Tables[0].Rows[0]["happyPoints"]).ToString();
                    //}
                    //catch
                    //{
                    //    lblCMLHP.InnerText = "0";
                    //}
                    Get_Yotpo_Points(Session["Session_YotpoEmail"].ToString());
                    try
                    {
                        lblCMLCredit.InnerText = Convert.ToInt32(pobj.Ds.Tables[0].Rows[0]["Credit"]).ToString();
                    }
                    catch
                    {
                        lblCMLCredit.InnerText = "0";
                    }
                }

                if (pobj.Ds.Tables[1].Rows.Count > 0)
                {
                    txtName.Text = pobj.Ds.Tables[1].Rows[0]["FirstName"].ToString() + " " + pobj.Ds.Tables[1].Rows[0]["LastName"].ToString();
                    txtEmailId.Text = pobj.Ds.Tables[1].Rows[0]["EmailID"].ToString();
                    txtPhone.Text = pobj.Ds.Tables[1].Rows[0]["PhoneNo"].ToString();
                }

            }
            #endregion

        }
        else
        {
            Response.Redirect("Default.aspx", false);
        }
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtName.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Plase Fill Name.', 'error')", true);
                return;
            }
            if (txtEmailId.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Plase Fill EmailId.', 'error')", true);
                return;
            }
            if (txtPhone.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Plase Fill Phone.', 'error')", true);
                return;
            }
            if (txtSubject.Text == "")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Plase Fill Subject.', 'error')", true);
                return;
            }
            PL_InvoiceDetail pobj = new PL_InvoiceDetail();
            pobj.CustID = Session["Session_UserloginID"].ToString();
            pobj.SysOrderNo = hdnSysOrderNo.Value;
            pobj.Name = txtName.Text;
            pobj.Email = txtEmailId.Text;
            pobj.Phone = txtPhone.Text;
            pobj.Subject = txtSubject.Text;
            pobj.Reason = txtReason.Text;

            string body = "";

            body += " <table style='border-collapse:collapse;  table-layout:fixed;  margin:0 auto;  border-spacing:0;  padding:0; height:100%!important; width:100%!important' width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>";
            body += "<tbody>";
            body += " <tr>";
            body += " <td style='border-collapse:collapse; background:#f8f8f8; padding-top:15px; padding-bottom:15px'>";
            body += " <table style='width:600px!important; min-width:600px!important; max-width:600px!important; margin:auto; border-spacing:0; border-collapse:collapse; background:white; border-radius:0px 0px 10px 10px; padding-left:30px; padding-right:30px; padding-top:30px; padding-bottom:30px; display:block' width='600' cellspacing='0' cellpadding='0' border='0' align='center'>";
            body += " <tbody>";
            body += " <tr>";
            body += " <td style='vertical-align:top; font-size:0; border-collapse:collapse'>";
            body += " <table style='border-spacing:0; border-collapse:collapse' width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#F8F8F8'>";


            body += " <tbody>";
            body += " <tr>";
            body += " <td style='padding-top:30px; padding-bottom:5px; background-color:white'>";
            body += " <span style='font-size:20px; color:#363636; '>Name :<b> " + pobj.Name + "</b> </span>";
            body += " </td>";
            body += " </tr>";
            body += " <tr>";
            body += " <td style='padding-top:30px; padding-bottom:5px; background-color:white'>";
            body += " <span style='font-size:20px; color:#363636; '>Email :<b> " + pobj.Email + "</b> </span>";
            body += " </td>";
            body += " </tr>";
            body += " <tr>";
            body += " <td style='padding-top:30px; padding-bottom:5px; background-color:white'>";
            body += " <span style='font-size:20px; color:#363636; '>Mobile :<b> " + pobj.Phone + "</b> </span>";
            body += " </td>";
            body += " </tr>";
            //body += " <tr>";
            //body += " <td style='padding-top:30px; padding-bottom:5px; background-color:white'>";
            //body += " <span style='font-size:20px; color:#363636; '>Subject :<b> " + pobj.Subject + "</b> </span>";
            //body += " </td>";
            //body += " </tr>";
            body += " <tr>";
            body += " <td style='padding-top:30px;padding-bottom:5px; background-color:white'>";
            body += " <span style='font-size:20px; color:#363636; '>Message :<b> " + pobj.Reason + "</b> </span>";
            body += " </td>";
            body += " </tr>";
            body += " <tr><td style='background:#ffffff; height:20px'></td></tr>";
            body += " </tbody>";
            body += " </table>";
            body += " </td>";
            body += " </tr>";
            body += " </tbody>";
            body += " </table>";
            body += " </td>";
            body += " </tr>";
            body += " </tbody>";
            body += " </table>";
            pobj.Body = body;


            BL_InvoiceDetail.CancelOrderRqst(pobj);
            if (!pobj.IsException)
            {
                CustomerActivity.CustomerActivityInsert("Customer has requested for cancel order", ViewState["Who"].ToString(), pobj.SysOrderNo.ToString(), DateTime.Now, DateTime.Now, 0, 0, 0, 0);
                bindgridReship();
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Cancel Request send successfully.', 'success')", true);

            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Cancel request is not send.', 'error')", true);
            }

        }
        catch (Exception ex)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Server Busy , Try After Some Time', 'error')", true);
        }
    }

    protected void lnkCancelProduct_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            hdnReshippLogNo.Value = (((Label)gr.FindControl("lnkReShipLogNo")).Text);
            hdnStateTax.Value = (((Label)gr.FindControl("lblStateTax")).Text);
            hdnInterval.Value = (((Label)gr.FindControl("LblreShipIntrvl")).Text);
            hdnNextShipDate.Value = (((Label)gr.FindControl("lblreNextreShipDate")).Text);
            hdnNoOfProduct.Value = (((Label)gr.FindControl("lnkNoOfProduct")).Text);
            GridSKUProduct.DataSource = null;
            GridSKUProduct.DataBind();
            ViewState["ContinuousOrderProduct"] = null;
            GridProduct.DataSource = null;
            GridProduct.DataBind();
            divproduct.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEChangeProduct", "PopupOpenMPEChangeProduct();", true);
        }
        catch
        {

        }
    }

    protected void btnProductSearch_Click(object sender, EventArgs e)
    {
        try
        {
            PL_InvoiceDetail pobj = new PL_InvoiceDetail();
            pobj.Prodcode = "";
            pobj.ProductName = txtProductName.Text;
            BL_InvoiceDetail.BindProCodeProName(pobj);
            if (!pobj.IsException)
            {
                if (pobj.Ds.Tables[0].Rows.Count > 0 && pobj.Ds.Tables != null)
                {
                    GridSKUProduct.DataSource = pobj.Ds.Tables[0];
                    GridSKUProduct.DataBind();
                    if (ViewState["ContinuousOrderProduct"] == null)
                    {
                        DataTable dt = pobj.Ds.Tables[0];
                        dt.Rows.Clear();
                        ViewState["ContinuousOrderProduct"] = dt;
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEChangeProduct1", "PopupOpenMPEChangeProduct();", true);
                }
                else
                {
                    GridSKUProduct.DataSource = null;
                    GridSKUProduct.DataBind();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEChangeProduct2", "PopupOpenMPEChangeProduct();", true);
                }
            }
        }
        catch (Exception ex)
        { }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            string SKuProductXml = "";
            decimal SPUnitPrice = 0;
            decimal SPQuantity = 0;
            int NoOfProduct = Convert.ToInt32(hdnNoOfProduct.Value);
            for (int i = 0; i < GridProduct.Rows.Count; i++)
            {

                try
                {
                    SPUnitPrice = Convert.ToDecimal((GridProduct.Rows[i].FindControl("lblMainYourPrice") as Label).Text.ToString());
                }
                catch
                {
                    SPUnitPrice = 0;
                }

                try
                {
                    SPQuantity = Convert.ToDecimal((GridProduct.Rows[i].FindControl("txtMainQty") as TextBox).Text.ToString());
                }
                catch
                {
                    SPQuantity = 0;
                }

                SKuProductXml += "<SKuProductXml>";
                SKuProductXml += "<SPUnitPrice><![CDATA[" + SPUnitPrice + "]]></SPUnitPrice>";
                SKuProductXml += "<SPDiscount><![CDATA[" + 0 + "]]></SPDiscount>";
                SKuProductXml += "<SPQuantity><![CDATA[" + SPQuantity + "]]></SPQuantity>";
                SKuProductXml += "<ProductID><![CDATA[" + (GridProduct.Rows[i].FindControl("lblMainProductID") as Label).Text.ToString() + "]]></ProductID>";
                SKuProductXml += "<Pd_ItemNo><![CDATA[" + (NoOfProduct + i + 1) + "]]></Pd_ItemNo>";
                SKuProductXml += "</SKuProductXml>";

            }
            if (SKuProductXml != "")
            {
                PL_InvoiceDetail pobj = new PL_InvoiceDetail();
                pobj.ReshipLogNo = Convert.ToInt32(hdnReshippLogNo.Value);
                pobj.SKuProductXml = SKuProductXml;
                pobj.Who = ViewState["Who"].ToString();
                BL_InvoiceDetail.InsertContinuousOrderProduct(pobj);
                if (!pobj.IsException)
                {
                    CustomerActivity.CustomerActivityInsert("Customer has addeed new product", ViewState["Who"].ToString(), pobj.ReshipLogNo.ToString(), DateTime.Now, DateTime.Now, 0, 0, 0, 0);
                    bindgridReship();
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Product added successfully.', 'success')", true);

                }
                else
                {
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Product is not add', 'error')", true);
                }
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Please added at least one product', 'error')", true);
            }
        }
        catch (Exception ex)
        { }
    }

    protected void btnAddProduct_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ContinuousOrderProduct"];
            bool flag = false;

            for (int i = 0; i < GridSKUProduct.Rows.Count; i++)
            {
                bool Productflag = true;
                if ((GridSKUProduct.Rows[i].FindControl("ChkProduct") as CheckBox).Checked == true)
                {
                    flag = true;
                    for (int k = 0; k < dt.Rows.Count; k++)
                    {
                        if (dt.Rows[k]["ProductID"].ToString() == (GridSKUProduct.Rows[i].FindControl("lblProductID2") as Label).Text.ToString())
                        {
                            Productflag = false;
                            break;
                        }
                    }
                    if (Productflag)
                    {
                        DataRow dr = dt.NewRow();

                        dr["ProductID"] = (GridSKUProduct.Rows[i].FindControl("lblProductID2") as Label).Text.ToString();
                        dr["ProdCode"] = GridSKUProduct.Rows[i].Cells[2].Text.ToString();
                        dr["ProductName"] = GridSKUProduct.Rows[i].Cells[3].Text.ToString();
                        dr["PunitPrice"] = (GridSKUProduct.Rows[i].FindControl("lblPunitPrice") as Label).Text.ToString();
                        dr["YourPrice"] = (GridSKUProduct.Rows[i].FindControl("lblYourPrice") as Label).Text.ToString();
                        dr["PQty"] = (GridSKUProduct.Rows[i].FindControl("lblPQty") as Label).Text;
                        dt.Rows.Add(dr);
                    }
                }

            }
            if (flag == false)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Please added at least one product', 'error')", true);
            }
            else
            {
                ViewState["ContinuousOrderProduct"] = dt;
                GridProduct.DataSource = dt;
                GridProduct.DataBind();
                //GridSKUProduct.DataSource = null;
                //GridSKUProduct.DataBind();
                divproduct.Visible = true;
                btnAddProduct.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEChangeProduct3", "PopupOpenMPEChangeProduct();", true);
            }


        }
        catch (Exception ex)
        { }
    }

    protected void imgminus_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            GridViewRow gr = ((sender as ImageButton).Parent.Parent) as GridViewRow;
            TextBox txtMainQty = gr.FindControl("txtMainQty") as TextBox;
            decimal Qty = 0;
            try
            {
                Qty = Convert.ToDecimal(txtMainQty.Text);
            }
            catch
            { }
            if (Qty > 1)
            {
                Qty = Qty - 1;
            }
            txtMainQty.Text = Qty.ToString();
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ContinuousOrderProduct"];
            for (int i = 0; i < GridProduct.Rows.Count; i++)
            {
                dt.Rows[i]["PQty"] = (GridProduct.Rows[i].FindControl("txtMainQty") as TextBox).Text.ToString();
            }
            ViewState["ContinuousOrderProduct"] = dt;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEChangeProduct5", "PopupOpenMPEChangeProduct();", true);
        }
        catch
        {

        }
    }

    protected void imgplus_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            GridViewRow gr = ((sender as ImageButton).Parent.Parent) as GridViewRow;
            TextBox txtMainQty = gr.FindControl("txtMainQty") as TextBox;
            decimal Qty = 0;
            try
            {
                Qty = Convert.ToDecimal(txtMainQty.Text);
            }
            catch
            { }
            Qty = Qty + 1;
            txtMainQty.Text = Qty.ToString();
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ContinuousOrderProduct"];
            for (int i = 0; i < GridProduct.Rows.Count; i++)
            {
                dt.Rows[i]["PQty"] = (GridProduct.Rows[i].FindControl("txtMainQty") as TextBox).Text.ToString();
            }
            ViewState["ContinuousOrderProduct"] = dt;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEChangeProduct4", "PopupOpenMPEChangeProduct();", true);
        }
        catch
        {

        }
    }

    protected void GridViewProduct_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                decimal Price = 0;
                decimal Qty = 0;
                decimal Total = 0;
                try
                {
                    Price = Convert.ToDecimal(((Label)e.Row.FindControl("lblViewYourPrice")).Text);
                }
                catch
                {
                }
                try
                {
                    Qty = Convert.ToDecimal(((Label)e.Row.FindControl("lblViewPQty")).Text);
                }
                catch
                {
                }
                Total = Price * Qty;
                Label lblViewTotal = e.Row.FindControl("lblViewTotal") as Label;
                lblViewTotal.Text = Total.ToString();
            }
        }
        catch { }
    }

    public void Calculation()
    {
        try
        {
            decimal SubTotal = 0;
            decimal StateTax = 0;
            decimal Taxamt = 0;
            decimal Total = 0;
            try
            {
                StateTax = Convert.ToDecimal(hdnStateTax.Value);
            }
            catch
            {

            }
            for (int i = 0; i < GridViewProduct.Rows.Count; i++)
            {
                try
                {
                    SubTotal = SubTotal + Convert.ToDecimal((GridViewProduct.Rows[i].FindControl("lblViewTotal") as Label).Text);
                }
                catch
                {

                }

            }
            txtSubTotal.Text = SubTotal.ToString("F2");
            txtTax.Text = StateTax.ToString();
            if (StateTax > 0)
            {
                Taxamt = ((SubTotal * StateTax) / 100);
                Total = SubTotal + Taxamt;
            }
            else
            {
                Total = SubTotal;
            }
            txtamt.Text = Taxamt.ToString("F2");
            txtTotal.Text = Total.ToString("F2");
        }
        catch
        {

        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            txtNextShipDateView.Text = hdnNextShipDate.Value;
            txtInterval.Text = hdnInterval.Value;
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ContinuousOrderProduct"];
            GridViewProduct.DataSource = dt;
            GridViewProduct.DataBind();
            Calculation();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEGridViewProduct", "PopupOpenMPEGridViewProduct();", true);

        }
        catch (Exception ex)
        { }
    }

    protected void btnConfirmmsg_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "MPEConfirmmationmsgChangeProduct", "MPEConfirmmationmsgChangeProduct();", true);
    }

    protected void btnUpdateInterval_Click(object sender, EventArgs e)
    {
        try
        {
            PropertyLayerConitnousOrder pobj = new PropertyLayerConitnousOrder();

            pobj.ReShippingNo = Int32.Parse(hdnReshippLogNo.Value);
            try
            {
                pobj.ShippingInterval = Convert.ToInt32(txtChangeInterval.Text);
            }
            catch
            {
                pobj.ShippingInterval = 0;
            }
            if (pobj.ShippingInterval != 0)
            {
                if (pobj.ShippingInterval <= 120)
                {

                    DateTime NxtshippingDate = (Convert.ToDateTime(hdnPreInvoiceDate.Value));
                    pobj.NxtshippingDate = NxtshippingDate.AddDays(pobj.ShippingInterval);
                    pobj.Status = hdnStatus.Value;
                    pobj.OrderType = "";
                    pobj.Who = ViewState["Who"].ToString();
                    BusinessContinousOrder.updateReshipIntr(pobj);
                    if (!pobj.IsException)
                    {
                        CustomerActivity.CustomerActivityInsert("Customer has changed Shipping Interval.", pobj.Who, pobj.ReShippingNo.ToString(), DateTime.Now, DateTime.Now, Convert.ToInt32(txtPreInterval.Text), pobj.ShippingInterval, 0, 0);
                        bindgridReship();
                        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Interval changed successfully.', 'success')", true);

                    }
                }
                else

                {
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Shipping Interval can change always less than 120 Days', 'error')", true);
                }
            }
            else

            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Shipping Interval is zero so please enter non zero value.', 'error')", true);
            }

        }
        catch (Exception ex)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Server Busy , Try After Some Time', 'error')", true);
        }
    }

    protected void lblbtnChangeInterval_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            hdnReshippLogNo.Value = (((Label)gr.FindControl("lnkReShipLogNo")).Text);
            txtPreInterval.Text = (((Label)gr.FindControl("LblreShipIntrvl")).Text);
            hdnStatus.Value = ((Label)gr.FindControl("lblreStatus")).Text.Trim();
            hdnPreInvoiceDate.Value = (((Label)gr.FindControl("lblrePrevreShipDate")).Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEMPEChangeInterval", "PopupOpenMPEMPEChangeInterval();", true);
        }
        catch
        {

        }
    }



    protected void lnkRemoveProduct_Click(object sender, EventArgs e)
    {

        try
        {
            GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            hdnReshippLogNo.Value = (((Label)gr.FindControl("lnkReShipLogNo")).Text);
            txtOrderNo.Text = (((Label)gr.FindControl("lblOrderNo")).Text);
            BindReshipProduct();
        }
        catch
        {
        }
    }

    public void BindReshipProduct()
    {
        try
        {
            PL_InvoiceDetail pobjCo = new PL_InvoiceDetail();
            {
                pobjCo.ReshipLogNo = Convert.ToInt32(hdnReshippLogNo.Value);
            }
            BL_InvoiceDetail.BindResipProduct(pobjCo);
            if (pobjCo.Ds != null && pobjCo.Ds.Tables.Count > 0 && pobjCo.Ds.Tables[0].Rows.Count > 0)
            {
                grdRemoveProduct.DataSource = pobjCo.Ds.Tables[0];
                grdRemoveProduct.DataBind();
            }
            else
            {
                grdRemoveProduct.DataSource = null;
                grdRemoveProduct.DataBind();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPERemoveProduct", "PopupOpenMPERemoveProduct();", true);
        }
        catch
        {

        }
    }

    protected void ImgDeleteTransaction_Click(object sender, ImageClickEventArgs e)
    {
        if (grdRemoveProduct.Rows.Count > 1)
        {
            ImageButton creditentryid = sender as ImageButton;
            hdnAutoId.Value = creditentryid.CommandArgument.Trim();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MPEConfirmmationmsgRemoveProduct", "MPEConfirmmationmsgRemoveProduct();", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MPEPaymentRequestDetail", "MPEPaymentRequestDetail();", true);
            // System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Product is not removed because one Product should exists in Subscribed', 'error')", true);
        }


    }

    protected void btnRemoveProduct_Click(object sender, EventArgs e)
    {
        try
        {
            PL_InvoiceDetail pobj = new PL_InvoiceDetail();

            pobj.AutoId = Convert.ToInt32(hdnAutoId.Value);
            pobj.ReshipLogNo = Convert.ToInt32(hdnReshippLogNo.Value);
            pobj.Who = ViewState["Who"].ToString();
            BL_InvoiceDetail.DeleteResipProduct(pobj);
            if (!pobj.IsException)
            {
                CustomerActivity.CustomerActivityInsert("Customer has removed product", ViewState["Who"].ToString(), pobj.ReshipLogNo.ToString(), DateTime.Now, DateTime.Now, 0, 0, 0, 0);
                BindReshipProduct();
                bindgridReship();
                //System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Removed Product successfully.', 'success')", true);

            }

            else

            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Product Is Not Removed', 'error')", true);
            }

        }
        catch (Exception ex)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Server Busy , Try After Some Time', 'error')", true);
        }
    }



    protected void btnUpdateHP_Click(object sender, EventArgs e)
    {
        try
        {
            //decimal AmtPerHappyPoint = 0;
            //decimal Happy_Points = 0;
            PL_InvoiceDetail pobjh = new PL_InvoiceDetail();
            string yotpomsg = "";
            if (Session["Session_YotpoEmail"].ToString() != "")
            {
                yotpomsg = ReddemGetCoupan();
            }
            //BL_InvoiceDetail.BindHappyPoints(pobjh);
            //if (pobjh.Ds.Tables[0].Rows.Count > 0)
            //{
            //    AmtPerHappyPoint = Convert.ToDecimal(pobjh.Ds.Tables[0].Rows[0]["AmntPerHappyPoints"].ToString());
            //}

            //decimal addHp = Convert.ToDecimal(txtAddHP.Text);
            //if (Convert.ToDecimal(txtAddHP.Text) > Convert.ToDecimal(txtAvailableHP.Text))
            //{
            //    txtAddHP.Text = "0";
            //    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','You cannot enter happy point more than available happy point', 'error')", true);
            //    return;
            //}
            if ((yotpomsg == "Success"))
            {
                PL_InvoiceDetail pobj = new PL_InvoiceDetail();
                pobj.AHappyPointAmount = Convert.ToDecimal(txtHappyMoney.Text);
                pobj.HappyPoints = Convert.ToDecimal(txtHappyPoints.Text);
                if (ddlRedemOption.SelectedIndex > 0 && txtRedeemCoupanCode.Text != "" && Session["Session_YotpoEmail"].ToString() != "")
                {
                    pobj.Yotpo_Redeem = ddlRedemOption.SelectedValue;
                    pobj.Yotpo_Redeem_Id = Convert.ToInt32(ddlRedemOption.SelectedValue);
                    pobj.Yotpo_Redeem_Points = Convert.ToInt32(hdnRedeemPoints.Value);
                    pobj.Yotpo_Redeem_Amount = Convert.ToInt32(txtCouponAmt.Text);
                    pobj.Yotpo_Redeem_Code = txtRedeemCoupanCode.Text;
                }
                pobj.ReshipLogNo = Convert.ToInt32(hdnReshippLogNo.Value);
                pobj.Who = ViewState["Who"].ToString();
                BL_InvoiceDetail.UpdateHP(pobj);
                if (!pobj.IsException)
                {
                    CustomerActivity.CustomerActivityInsert("Customer has added Happy Points.", ViewState["Who"].ToString(), pobj.ReshipLogNo.ToString(), DateTime.Now, DateTime.Now, 0, 0, Convert.ToDecimal(txtHappyPoints.Text), 0);
                    bindform();
                    bindgridReship();
                    BindPendingHappyPoints();
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Happy Points added successfully.', 'success')", true);

                }

                else

                {
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','" + pobj.ExceptionMessage + "', 'error')", true);
                }
                txtHPNewNextInvoiceAmt.Text = pobj.Ds.Tables[0].Rows[0]["New_NextInvoiceAmt"].ToString();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEMPEAddHP2", "PopupOpenMPEMPEAddHP();", true);
        }
        catch
        {
        }
    }

    protected void lnkHappyPoint_Click(object sender, EventArgs e)
    {
        GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
        hdnReshippLogNo.Value = (((Label)gr.FindControl("lnkReShipLogNo")).Text);
        txtAvailableHP.Text = lblAvailableHP.InnerText;
        txtHPNextInvoiceAmt.Text = (((Label)gr.FindControl("lblretotalamt")).Text);
        txtHPNewNextInvoiceAmt.Text = (((Label)gr.FindControl("lblretotalamt")).Text);
        ddlRedemOption.SelectedIndex = 0;
        //txtAddHP.Text = (gr.FindControl("lblHappyPoint") as Label).Text;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEMPEAddHP1", "PopupOpenMPEMPEAddHP();", true);
    }

    private void BindPendingHappyPoints()
    {
        try
        {
            PL_InvoiceDetail pobjCo = new PL_InvoiceDetail();
            {
                pobjCo.CustID = Session["Session_UserloginID"].ToString();
            }
            BL_InvoiceDetail.BindPendingHappyPoints(pobjCo);
            decimal HP = 0, AvalHP = 0, PDHP = 0;
            if (pobjCo.Ds != null && pobjCo.Ds.Tables.Count > 0 && pobjCo.Ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    PDHP = Convert.ToDecimal(pobjCo.Ds.Tables[0].Rows[0]["PenidngHpAmt"]);
                }
                catch
                {
                }

            }
            HP = Convert.ToDecimal(lblAvailableHP.InnerText);
            AvalHP = HP + PDHP;
            lblPendingHP.InnerText = PDHP.ToString();
            lblCMLHP.InnerText = AvalHP.ToString();
        }
        catch { }
    }

    private void BindPendingCredit()
    {
        try
        {
            PL_InvoiceDetail pobjCo = new PL_InvoiceDetail();
            {
                pobjCo.CustID = Session["Session_UserloginID"].ToString();
            }
            BL_InvoiceDetail.BindPendingCredit(pobjCo);
            decimal CG = 0, AvalCG = 0, PDCG = 0;
            if (pobjCo.Ds != null && pobjCo.Ds.Tables.Count > 0 && pobjCo.Ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    PDCG = Convert.ToDecimal(pobjCo.Ds.Tables[0].Rows[0]["PendingCredit"]);
                }
                catch
                {
                }

            }
            CG = Convert.ToDecimal(lblCMLCredit.InnerText);
            AvalCG = CG - PDCG;
            lblPenidngCredit.InnerText = PDCG.ToString();
            lblavailableCredit.InnerText = AvalCG.ToString();
        }
        catch { }
    }

    protected void lnkCreditGiven_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            hdnReshippLogNo.Value = (((Label)gr.FindControl("lnkReShipLogNo")).Text);
            txtAvailableCredit.Text = lblavailableCredit.InnerText;
            txtCG_NextInvoiceAmt.Text = (((Label)gr.FindControl("lblretotalamt")).Text);
            txtCG_New_NextInvoiceAmt.Text = (((Label)gr.FindControl("lblretotalamt")).Text);
            txtaddcredit.Text = (gr.FindControl("lblCreditGiven") as Label).Text;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEAddCreditGiven1", "PopupOpenMPEAddCreditGiven();", true);
        }
        catch
        {

        }
    }

    protected void btnUpdateCredit_Click(object sender, EventArgs e)
    {
        try
        {

            if (Convert.ToDecimal(txtaddcredit.Text) > Convert.ToDecimal(txtAvailableCredit.Text))
            {
                txtaddcredit.Text = "0";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','You cannot enter credit more than available credit', 'error')", true);
                return;
            }

            PL_InvoiceDetail pobj = new PL_InvoiceDetail();
            pobj.CreditGiven = Convert.ToDecimal(txtaddcredit.Text);
            pobj.ReshipLogNo = Convert.ToInt32(hdnReshippLogNo.Value);
            pobj.Who = ViewState["Who"].ToString();
            BL_InvoiceDetail.UpdateCredit(pobj);
            if (!pobj.IsException)
            {
                CustomerActivity.CustomerActivityInsert("Customer has added Credit.", ViewState["Who"].ToString(), pobj.ReshipLogNo.ToString(), DateTime.Now, DateTime.Now, 0, 0, 0, Convert.ToDecimal(txtaddcredit.Text));
                bindform();
                bindgridReship();
                BindPendingCredit();
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Credit added successfully.', 'success')", true);

            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','" + pobj.ExceptionMessage + "', 'error')", true);
            }
            txtCG_New_NextInvoiceAmt.Text = pobj.Ds.Tables[0].Rows[0]["New_NextInvoiceAmt"].ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEAddCreditGiven2", "PopupOpenMPEAddCreditGiven();", true);
        }
        catch
        {
        }
    }

    protected void lnkShippingAddress_Click(object sender, EventArgs e)
    {
        bindpopShipGrid();
        GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
        hdnReshippLogNo.Value = (((Label)gr.FindControl("lnkReShipLogNo")).Text);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEShipAddress1", "PopupOpenMPEShipAddress();", true);
    }

    private void bindpopShipGrid()
    {
        try
        {
            PL_CustomerDetail pobj = new PL_CustomerDetail();
            pobj.CustID = ViewState["Who"].ToString();
            BL_CustomerDetail.BindShippingAddressList(pobj);
            ViewState["dtgrdShippop"] = pobj.Ds.Tables[0];
            GridShipPop.DataSource = pobj.Ds.Tables[0];
            GridShipPop.DataBind();
        }
        catch { }
    }

    protected void ddlnewS_country_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bindddlnewB_State(ddlnewS_country, ddlnewS_state);
            bindddlnewB_City(ddlnewS_state, ddlnewS_city);
            txtnewS_zip.Text = null;
        }

        catch { }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPENewShippingAddres16", "PopupOpenMPENewShippingAddres();", true);
    }

    protected void ddlnewS_state_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEShipAddress7", "PopupOpenMPEShipAddress();", true);
            bindddlnewB_City(ddlnewS_state, ddlnewS_city);
            txtnewS_zip.Text = null;
        }
        catch { }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPENewShippingAddres15", "PopupOpenMPENewShippingAddres();", true);
    }

    protected void ddlnewS_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlnewS_city.SelectedIndex > 0)
            {

                string[] cityS = ddlnewS_city.SelectedItem.Text.Split('(');
                string[] zipS = cityS[1].Split(')');
                string zip1 = zipS[0];
                if (hdnIsTxtnewS_zip_TextChanged.Value == "0")
                {
                    txtnewS_zip.Text = zip1;
                }
                else
                {
                    DataUtility bobj = new DataUtility();
                    bobj.SetDDSelectedItemByText(ddlnewS_city, ddlnewS_city.SelectedItem.Value + " (" + txtnewS_zip.Text + ")");
                    hdnIsTxtnewB_zipcode_TextChanged.Value = "0";
                }
                //txtnewS_zip.Text = ddlnewS_city.SelectedItem.Text.Substring(ddlnewS_city.SelectedItem.Text.Length - 6, 5);
            }
            else
            {
                txtnewS_zip.Text = null;

            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPENewShippingAddres12", "PopupOpenMPENewShippingAddres();", true);
        }
        catch { }
    }
    protected void txtnewS_zip_TextChanged(object sender, EventArgs e)
    {
        try
        {
            PL_CustomerDetail pobj = new PL_CustomerDetail();
            DataUtility bobj = new DataUtility();
            pobj.zipcode = txtnewS_zip.Text.Trim();
            BL_CustomerDetail.selectDetailsByzipcode(pobj);
            if (!pobj.IsException)
            {
                if (pobj.Ds.Tables[0].Rows.Count > 0)
                {
                    hdnIsTxtnewS_zip_TextChanged.Value = "1";
                    bobj.SetDDSelectedItemByText(ddlnewS_country, pobj.Ds.Tables[0].Rows[0]["countryName"].ToString());

                    bindddlnewB_State(ddlnewS_country, ddlnewS_state);
                    bobj.SetDDSelectedItemByText(ddlnewS_state, pobj.Ds.Tables[0].Rows[0]["stateName"].ToString());

                    bindddlnewB_City(ddlnewS_state, ddlnewS_city);
                    bobj.SetDDSelectedItemByText(ddlnewS_city, pobj.Ds.Tables[0].Rows[0]["CityZip"].ToString());
                    txtnewS_phno.Focus();
                }
                else
                {
                    txtnewS_zip.Text = null;
                    ddlnewS_state.SelectedIndex = 0;
                    ddlnewS_city.SelectedIndex = 0;
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Current ZipCode does not exist in this system..Go to 'Sale Tax Master' to store new Zipcode..', 'error')", true);
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPENewShippingAddres15", "PopupOpenMPENewShippingAddres();", true);
        }
        catch { }
    }

    private void bindddlnewB_country(DropDownList ddlCountry)
    {
        try
        {
            PL_CustomerDetail pobj = new PL_CustomerDetail();
            BL_CustomerDetail.selectCountry(pobj);

            ddlCountry.DataTextField = "countryName";
            ddlCountry.DataValueField = "CountryAbbr";
            ddlCountry.DataSource = pobj.Ds.Tables[0];
            ddlCountry.Text = "USA";
            ddlCountry.DataBind();

        }
        catch { }

    }

    private void bindddlnewB_State(DropDownList ddlCountry, DropDownList ddlState)
    {
        try
        {
            PL_CustomerDetail pobj = new PL_CustomerDetail();
            pobj.countryAbbr = ddlCountry.SelectedValue;
            BL_CustomerDetail.selectStateByCountry(pobj);

            ddlState.DataTextField = "stateName";
            ddlState.DataValueField = "stateabbr";
            ddlState.DataSource = pobj.Ds.Tables[0];
            ddlState.DataBind();
            ddlState.Items.Insert(0, "Select State");

        }
        catch { }
    }

    private void bindddlnewB_City(DropDownList ddlState, DropDownList ddlCity)
    {
        try
        {
            if (ddlState.SelectedIndex > 0)
            {
                PL_CustomerDetail pobj = new PL_CustomerDetail();
                pobj.stateAbbr = ddlState.SelectedValue;
                BL_CustomerDetail.selectCityZIPByState(pobj);

                ddlCity.DataTextField = "cityZip";
                ddlCity.DataValueField = "cityName";
                ddlCity.DataSource = pobj.Ds.Tables[0];
            }
            else
            {
                ddlCity.DataSource = null;
            }
            ddlCity.DataBind();
            ddlCity.Items.Insert(0, "Select City");

        }
        catch { }
    }

    protected void btnnewshippingA_Click(object sender, EventArgs e)
    {
        try
        {
            PL_CustomerDetail pobj = new PL_CustomerDetail();
            pobj.SAddress1 = txtnewS_add1.Text.Trim();
            pobj.SAddress2 = txtnewS_add2.Text.Trim();
            pobj.SCountry = ddlnewS_country.SelectedItem.Text.Trim();
            pobj.SState = ddlnewS_state.SelectedItem.Text.Trim();
            pobj.SCity = ddlnewS_city.SelectedValue;

            pobj.SZip = txtnewS_zip.Text.Trim();
            pobj.CustID = ViewState["Who"].ToString();
            pobj.FirstName = txtS_Fname.Text.Trim();
            pobj.LastName = txtS_Lname.Text.Trim();
            pobj.STCompany = txtnewS_Company.Text.Trim();
            pobj.OfficeNoS = txtnewS_OfficeNo.Text.Trim();
            pobj.SEmailID = txtnewS_email.Text.Trim();
            pobj.SPhone = txtnewS_phno.Text.Trim();
            pobj.SPhone1 = txtnewS_Phone2.Text.Trim();

            {
                pobj.Who = ViewState["Who"].ToString();
            }
            BL_CustomerDetail.InsertShippingAddress(pobj);
            if (pobj.IsException)
            {
                lblmsg2.Style.Add("color", "red");
                lblmsg2.Visible = true;
                lblmsg2.Text = pobj.ExceptionMessage;

            }
            else
            {
                lblmsg2.Visible = false;
                SetDefaultShipping(pobj.Ds.Tables[0].Rows[0]["ShippingEntryID"].ToString());
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','New Shipping Address saved Successfully', 'success')", true);
                CustomerActivity.CustomerActivityInsert("Customer has added New Shipping Address.", ViewState["Who"].ToString(), hdnReshippLogNo.Value.ToString(), DateTime.Now, DateTime.Now, 0, 0, 0, 0);
                bindpopShipGrid();
                ResetPopShip();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MPENewShipAddressPopupClosedBtnClick3", "MPENewShipAddressPopupClosedBtnClick();", true);
        }
        catch { }
    }


    protected void btnnewshippingCancel_Click(object sender, EventArgs e)
    {
        // ResetPopShip();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEShipAddress1", "PopupOpenMPEShipAddress();", true);
        bindpopShipGrid();
        ResetPopShip();
    }

    protected void btnsaveShipping_OnClick(object sender, EventArgs e)
    {
        try
        {
            bool flag = false;
            foreach (GridViewRow gr in GridShipPop.Rows)
            {
                CheckBox chk = (CheckBox)gr.FindControl("chkdefault1");
                if (chk.Checked == true)
                {
                    string ShippingEntryId = ((ImageButton)(gr.FindControl("LnkShipId"))).CommandArgument.Trim();
                    flag = SetDefaultShipping(ShippingEntryId);
                    if (flag)
                    {
                        bindform();
                        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Detail saved successfully', 'success')", true);
                        CustomerActivity.CustomerActivityInsert("Customer has set address for cusromer details.", ViewState["Who"].ToString(), hdnReshippLogNo.Value.ToString(), DateTime.Now, DateTime.Now, 0, 0, 0, 0);
                    }
                    else

                        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Server busy. Detail can't be save this time!!!', 'error')", true);
                    break;
                }
            }
        }
        catch { }
    }
    protected bool SetDefaultShipping(string ShippingEntryId)
    {
        bool flag = false; try
        {
            PL_CustomerDetail pobj = new PL_CustomerDetail();
            pobj.CustID = ViewState["Who"].ToString();
            pobj.ShippingEntryID = ShippingEntryId;
            pobj.Who = ViewState["Who"].ToString();
            BL_CustomerDetail.updateshippop(pobj);
            if (!pobj.IsException)
            {
                flag = true;
            }
            if (pobj.IsException)
            {
                flag = false;
            }
            return flag;
        }
        catch { return flag; }
    }

    protected void btnShipUsethis_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow gr in GridShipPop.Rows)
            {
                CheckBox chk = (CheckBox)gr.FindControl("chkdefault1");
                if (chk.Checked == true)
                {
                    string ShippingEntryID = ((ImageButton)(gr.FindControl("LnkShipId"))).CommandArgument.Replace("&nbsp;", "");
                    PL_InvoiceDetail pobj = new PL_InvoiceDetail();
                    pobj.CustID = ViewState["Who"].ToString();
                    pobj.ShippingEntryID = ShippingEntryID;
                    pobj.Who = ViewState["Who"].ToString();
                    pobj.ReshipLogNo = Convert.ToInt32(hdnReshippLogNo.Value);
                    BL_InvoiceDetail.UpdateShipAddress(pobj);
                    if (!pobj.IsException)
                    {
                        bindform();
                        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Detail added successfully', 'success')", true);
                        CustomerActivity.CustomerActivityInsert("Customer has set address for order.", ViewState["Who"].ToString(), hdnReshippLogNo.Value.ToString(), DateTime.Now, DateTime.Now, 0, 0, 0, 0);
                    }
                    else
                    {
                        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Server busy. Detail can't be save this time!!!', 'error')", true);
                    }

                    break;
                }
            }
        }

        catch (Exception ex)
        {
            //MPEMessageStyle(1, "some thing wrong please contact the administration");
        }

    }


    protected void btn_addnewShipng_Click(object sender, EventArgs e)
    {
        try
        {
            ResetPopShip();
            bindddlnewB_country(ddlnewS_country);
            bindddlnewB_State(ddlnewS_country, ddlnewS_state);
            bindddlnewB_City(ddlnewS_state, ddlnewS_city);
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MPEShipAddress", "$('#MPEShipAddress').modal('hide');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MPEShipAddressPopupClosedBtnClick1", "MPEShipAddressPopupClosedBtnClick();", true);
            // btnnewshippingA.Visible = true;
        }
        catch { }
    }
    protected void btnclosepopshipping_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MPEShipAddress", "$('#MPEShipAddress').modal('hide');", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPENewShippingAddres1", "PopupOpenMPENewShippingAddres();", true);
    }

    protected void ResetPopShip()
    {
        try
        {
            txtS_Fname.Text = "";
            txtS_Lname.Text = "";
            txtnewS_Company.Text = "";
            txtnewS_add1.Text = "";
            txtnewS_add2.Text = "";

            ddlnewS_country.Text = null;

            ddlnewS_state.Text = null;

            ddlnewS_city.Text = null;
            txtnewS_email.Text = "";
            txtnewS_phno.Text = "";
            txtnewS_Phone2.Text = "";
            txtnewS_zip.Text = "";

        }
        catch { }
    }


    protected void btnnewshippingUseThisAdd_Click(object sender, EventArgs e)
    {
        try
        {
            PL_CustomerDetail pobj = new PL_CustomerDetail();
            pobj.SAddress1 = txtnewS_add1.Text.Trim();
            pobj.SAddress2 = txtnewS_add2.Text.Trim();
            pobj.SCountry = ddlnewS_country.SelectedItem.Text.Trim();
            pobj.SState = ddlnewS_state.SelectedItem.Text.Trim();
            pobj.SCity = ddlnewS_city.SelectedValue;

            pobj.SZip = txtnewS_zip.Text.Trim();
            pobj.CustID = ViewState["Who"].ToString();
            pobj.FirstName = txtS_Fname.Text.Trim();
            pobj.LastName = txtS_Lname.Text.Trim();
            pobj.STCompany = txtnewS_Company.Text.Trim();
            pobj.OfficeNoS = txtnewS_OfficeNo.Text.Trim();
            pobj.SEmailID = txtnewS_email.Text.Trim();
            pobj.SPhone = txtnewS_phno.Text.Trim();
            pobj.SPhone1 = txtnewS_Phone2.Text.Trim();

            {
                pobj.Who = ViewState["Who"].ToString();
            }
            BL_CustomerDetail.InsertShippingAddress(pobj);
            if (pobj.IsException)
            {
                lblmsg2.Style.Add("color", "red");
                lblmsg2.Visible = true;
                lblmsg2.Text = pobj.ExceptionMessage;

            }
            else
            {
                lblmsg2.Visible = false;
                PL_InvoiceDetail obj = new PL_InvoiceDetail();
                obj.CustID = ViewState["Who"].ToString();
                obj.ShippingEntryID = pobj.Ds.Tables[0].Rows[0]["ShippingEntryID"].ToString();
                obj.Who = ViewState["Who"].ToString();
                obj.ReshipLogNo = Convert.ToInt32(hdnReshippLogNo.Value);
                BL_InvoiceDetail.UpdateShipAddress(obj);
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','New Shipping Address saved Successfully', 'success')", true);
                CustomerActivity.CustomerActivityInsert("Customer has added New Shipping Address.", ViewState["Who"].ToString(), hdnReshippLogNo.Value.ToString(), DateTime.Now, DateTime.Now, 0, 0, 0, 0);
                bindpopShipGrid();
                ResetPopShip();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MPENewShipAddressPopupClosedBtnClick1", "MPENewShipAddressPopupClosedBtnClick();", true);
        }
        catch { }
    }

    protected void btnclosepopnewshipping_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MPENewShippingAddres", "$('#MPENewShippingAddres').modal('hide');", true);
    }

    #region Yotpo
    public void Get_Yotpo_Points(string YotpoPoints)
    {
        try
        {
            if (YotpoPoints != "")
            {
                lblAvailableHP.InnerText = YotpoAPI.FetchCustomerYotpoPoints(YotpoPoints);
            }
            else
            {
                lblAvailableHP.InnerText = "0";
            }
        }
        catch
        {

        }
    }

    private void FetchRedeemOptionYotpo()
    {
        try
        {
            string response = YotpoAPI.FetchRedeemOptionYotpo();
            JavaScriptSerializer js = new JavaScriptSerializer();
            RedeemOption[] persons = js.Deserialize<RedeemOption[]>(response);
            //ViewState["persons"] = persons;
            DataTable _myDataTable = new DataTable();
            _myDataTable.Columns.Add(new DataColumn("id"));
            _myDataTable.Columns.Add(new DataColumn("name"));
            _myDataTable.Columns.Add(new DataColumn("amount"));
            for (int i = 0; i < persons.Length; i++)
            {
                DataRow row = _myDataTable.NewRow();
                row["id"] = persons[i].id;
                row["name"] = persons[i].name;
                row["amount"] = persons[i].amount;
                _myDataTable.Rows.Add(row);
            }//
            Session["RedeemOption"] = _myDataTable;
            ddlRedemOption.DataTextField = "name";
            ddlRedemOption.DataValueField = "id";
            ddlRedemOption.DataSource = _myDataTable;
            ddlRedemOption.DataBind();
            ddlRedemOption.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Redeem-", "0"));
        }
        catch
        { }
    }

    protected string ReddemGetCoupan()
    {

        string Msg = "";
        try
        {
            // txtemail.Text = "caorihuela@gmail.com";
            txtRedeemCoupanCode.Text = "";
            txtCouponAmt.Text = "0.00";
            txtHappyMoney.Text = "0";
            txtHappyPoints.Text = "0";
            hdnRedeemPoints.Value = "0";

            if (ddlRedemOption.SelectedValue == "0")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Please select Redeem option.', 'error')", true);
                ddlRedemOption.Focus();
                return Msg;
            }
            DataTable dt = new DataTable();
            dt = Session["RedeemOption"] as DataTable;
            int Amount = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["id"].ToString() == ddlRedemOption.SelectedValue)
                {
                    Amount = Convert.ToInt32(dt.Rows[i]["amount"].ToString());
                    break;
                }

            }
            if (GridReShipList.Rows.Count == 0)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Error", "swal('Success','Please select at least one sku.', 'error')", true);
                ddlRedemOption.SelectedIndex = 0;
                return Msg;
            }
            txtHappyPoints.Text = (Amount).ToString();
            txtHappyMoney.Text = Convert.ToInt32(Amount / 10).ToString();
            CalculationHP();
            if (Convert.ToDecimal(txtHPNextInvoiceAmt.Text) < 0)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','Total amount should not be minus', 'error')", true);
                ddlRedemOption.SelectedIndex = 0;
                txtHappyPoints.Text = "0";
                txtHappyMoney.Text = "0";
                CalculationHP();
                return Msg;
            }

            Msg = YotpoAPI.CreateRedeem_YotpoPoints(Session["Session_YotpoEmail"].ToString(), Convert.ToInt32(ddlRedemOption.SelectedValue));
            if (Msg == "redemption option was not found")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Error", "swal('Success','" + Msg + "', 'error')", true);
                ddlRedemOption.SelectedIndex = 0;
                txtHappyPoints.Text = "0";
                txtHappyMoney.Text = "0";
                CalculationHP();
                return Msg;
            }
            else if (Msg == "Not Found")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Error", "swal('Success','" + Msg + "', 'error')", true);
                ddlRedemOption.SelectedIndex = 0;
                txtHappyPoints.Text = "0";
                txtHappyMoney.Text = "0";
                CalculationHP();
                return Msg;
            }
            else if (Msg == "You don't have enough points for this amount")
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Error", "swal('Success','You dont have enough points for this amount', 'error')", true);
                ddlRedemOption.SelectedIndex = 0;
                txtHappyPoints.Text = "0";
                txtHappyMoney.Text = "0";
                CalculationHP();
                return Msg;
            }
            else if (Msg != "")
            {
                txtRedeemCoupanCode.Text = Msg;
                hdnRedeemPoints.Value = Amount.ToString();
                if (Amount > 0)
                {
                    txtCouponAmt.Text = Convert.ToInt32(Amount / 10).ToString();

                }
                else
                {
                    txtCouponAmt.Text = Amount.ToString();

                }
                Msg = "Success";

            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Error", "swal('Success','" + Msg + "', 'error')", true);
            }
            return Msg;

        }
        catch
        {
            return Msg;
        }
    }

    protected void ddlRedemOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRedemOption.SelectedIndex > 0)
        {
            DataTable dt = new DataTable();
            dt = Session["RedeemOption"] as DataTable;
            int Amount = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["id"].ToString() == ddlRedemOption.SelectedValue)
                {
                    Amount = Convert.ToInt32(dt.Rows[i]["amount"].ToString());
                    break;
                }

            }
            txtHappyPoints.Text = (Amount).ToString();
            txtHappyMoney.Text = Convert.ToInt32(Amount / 10).ToString();
            txtCouponAmt.Text = Convert.ToInt32(Amount / 10).ToString();
        }
        else
        {
            txtHappyPoints.Text = "0";
            txtHappyMoney.Text = "0";
        }
        CalculationHP();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEMPEAddHP3", "PopupOpenMPEMPEAddHP();", true);
    }

    public void CalculationHP()
    {
        decimal HPAmt = Convert.ToDecimal(txtHappyMoney.Text);
        decimal NextInvoiceAmt = Convert.ToDecimal(txtHPNextInvoiceAmt.Text);
        txtHPNewNextInvoiceAmt.Text = (NextInvoiceAmt - HPAmt).ToString("F2");
    }
    #endregion
}

public class RedeemOption
{
    public int id;
    public string name;
    public int amount;
}