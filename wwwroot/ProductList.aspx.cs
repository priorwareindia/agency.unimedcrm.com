﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Web.Services;
using DLLInvoiceDetail;


public partial class ProductList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindOrderProductGrid();
        }
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    private void bindOrderProductGrid()
    {
        try
        {
            PL_InvoiceDetail pobj = new PL_InvoiceDetail();
            if (Session["Session_UserloginID"].ToString() != "")
            {
                pobj.CustID = Session["Session_UserloginID"].ToString();
            }
            BL_InvoiceDetail.selectProductSbyCustId(pobj);
            ViewState["GridProductLog"] = pobj.Ds.Tables[0];
            GridProductLog.DataSource = pobj.Ds.Tables[0];
            GridProductLog.DataBind();

        }
        catch { }
    }
    protected void GridProductLog_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortingDirection = string.Empty;
            if (dir == SortDirection.Ascending)
            {
                dir = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                dir = SortDirection.Ascending;
                sortingDirection = "Asc";
            }
            DataView sortedView = new DataView(sortbindgrid3());
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            GridProductLog.DataSource = sortedView;
            GridProductLog.DataBind();

        }
        catch { }
    }
    protected DataTable sortbindgrid3()
    {

        DataTable dt = ViewState["GridProductLog"] as DataTable;
        return dt;
    }
    protected void GridProductLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            for (int rowIndex = GridProductLog.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = GridProductLog.Rows[rowIndex];
                GridViewRow previousRow = GridProductLog.Rows[rowIndex + 1];
                if (((Label)row.FindControl("lnkProInvoiceNo")).Text == ((Label)previousRow.FindControl("lnkProInvoiceNo")).Text && ((Label)row.FindControl("lnkProSkuID")).Text == ((Label)previousRow.FindControl("lnkProSkuID")).Text)
                {
                    for (int i = 0; i < row.Cells.Count; i++)
                    {
                        string headerRowText = GridProductLog.HeaderRow.Cells[i].Text.Trim();
                        if (!(headerRowText == "Product" || headerRowText == "Product Quantity" || headerRowText == "Product Price"))
                        {
                            if (row.Cells[i].Text == previousRow.Cells[i].Text)
                            {
                                row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                                previousRow.Cells[i].Visible = false;
                                if (previousRow.Cells[i].RowSpan >= 2)
                                {
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
}