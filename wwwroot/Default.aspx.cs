﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblmessage.Visible = false;
            Session.Abandon(); // Does nothing
            Session.Clear();

        }
    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {
        try
        {
            lblmessage.Visible = false;
            string AutoId, AgencyName, ContactPeronName;
            PL_AgencyLogin pobj = new PL_AgencyLogin();
            DataUtility dobj = new DataUtility();
            pobj.LoginID = username.Text.Trim();
            pobj.Password = dobj.EncodePasswordToBase64(password.Text);
            pobj.IpAddress = HttpContext.Current.Request.UserHostAddress;
            BL_AgencyLogin.Login(pobj);
            if (!pobj.IsException && pobj.Dt.Rows.Count == 1)
            {
                AutoId = pobj.Dt.Rows[0]["AutoId"].ToString();
                AgencyName = pobj.Dt.Rows[0]["AgencyName"].ToString();
                ContactPeronName = pobj.Dt.Rows[0]["ContactPeronName"].ToString();

                Session.Add("AutoId", AutoId);
                Session.Add("AgencyName", AgencyName);
                Session.Add("ContactPeronName", ContactPeronName);
                Response.Redirect("Home.aspx", false);

            }
            else
            {

                lblmessage.Text = "Unable to proceed please fill correct Email Id and Password!!!";
                lblmessage.Visible = true;

            }

        }
        catch (Exception ex)
        { }
    }

    //protected void Unnamed_Click(object sender, EventArgs e)
    //{
    //    txtForgotUserId.Text = "";
    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "MPEForgotPassword", "MPEForgotPassword();", true);
    //}

    //protected void btnForgotPassword_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        PL_Login pob = new PL_Login();
    //        pob.EmailID = txtForgotUserId.Text;
    //        pob.IpAddress = HttpContext.Current.Request.UserHostAddress;
    //        BL_Login.ForgotPassword(pob);
    //        if (!pob.IsException)
    //        {
    //            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Success','Forgot Passowrd is successfully.', 'success')", true);
    //        }
    //        else if (pob.IsException == true && pob.ExceptionMessage == "")
    //        {
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "MPEPaymentRequestDetail1", "MPEPaymentRequestDetail();", true);
    //        }
    //        else
    //        {
    //            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "swal('Error','" + pob.ExceptionMessage + "', 'error')", true);
    //        }
    //    }
    //    catch
    //    {

    //    }
    //}
}