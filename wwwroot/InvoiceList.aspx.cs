﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Web.Services;
using DLLInvoiceDetail;

public partial class InvoiceList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindInvoiceGrid();
        }
    }

    private void bindInvoiceGrid()
    {
        try
        {
            PL_InvoiceDetail pobj = new PL_InvoiceDetail(); 
            if (Session["Session_UserloginID"].ToString() != "")
            {
                pobj.CustID = Session["Session_UserloginID"].ToString();
            }
            BL_InvoiceDetail.selectinvGridbyCustID(pobj);
            if (Session["Session_UserloginID"].ToString() != "")
            {
                ViewState["dtinvoce"] = pobj.Ds.Tables[0];
            }
            GridInvoice.DataSource = pobj.Ds.Tables[0];
            GridInvoice.DataBind();
        }
        catch (Exception ex) { }
    }

    protected void GridInvoice_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortingDirection = string.Empty;
            if (dir == SortDirection.Ascending)
            {
                dir = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                dir = SortDirection.Ascending;
                sortingDirection = "Asc";
            }
            DataView sortedView = new DataView(sortbindgrid());
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
        }
        catch (Exception ex) { }
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected DataTable sortbindgrid()
    {
        DataTable dt = ViewState["dtinvoce"] as DataTable;
        return dt;
    }

    protected void btnTrackingNo_Click(object sender, EventArgs e)
    {
        try
        {
            PL_InvoiceDetail pobj = new PL_InvoiceDetail();
            foreach (GridViewRow grs in GridInvoice.Rows)
            { grs.Cells[0].CssClass = "GridviewScrollItem"; }
            GridViewRow gr2 = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            string TrackingUrl = ((Label)gr2.FindControl("TrackingUrl")).Text;
            string Trackingno = (sender as LinkButton).CommandArgument;
            string url = TrackingUrl + Trackingno;
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "window.open('" + url + "');", true); ;
        }
        catch (Exception ex)
        {

        }
    }

    protected void GridInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton TrackingUrl = ((LinkButton)e.Row.FindControl("lnkTrackingNo"));
                string strackurl = TrackingUrl.Attributes["value"].ToString();
                if (strackurl == "")
                {
                    ((LinkButton)e.Row.FindControl("lnkTrackingNo")).Visible = false;
                }
                ((Label)e.Row.FindControl("DeliveredDate")).Text = ((Label)e.Row.FindControl("DeliveredDate")).Text.Replace("And", "");
            }
        }
        catch (Exception ex)
        { }
    }

    protected void lbtnViewInvoice_Click(object sender, EventArgs e)
    {

        try
        {
            string Total = "0";
            GridViewRow gr = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            string Invoiceno = (((Label)gr.FindControl("lnkInvoiceNo")).Text);
            PL_InvoiceDetail pobj = new PL_InvoiceDetail();
            pobj.SysOrderNo = Invoiceno;
            BL_InvoiceDetail.BindViewInvoice(pobj);
            if (pobj.Ds != null && pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[1].Rows.Count>0)
            {
                GridViewProduct.DataSource = pobj.Ds.Tables[1];
                GridViewProduct.DataBind();
                txtPaymentType.Text = pobj.Ds.Tables[0].Rows[0]["PrintPM"].ToString();
                Total = (Convert.ToDecimal(pobj.Ds.Tables[0].Rows[0]["Subtotalamt"].ToString()) - Convert.ToDecimal(pobj.Ds.Tables[0].Rows[0]["CoupanAmount"].ToString()) - Convert.ToDecimal(pobj.Ds.Tables[0].Rows[0]["DiscountAmt"].ToString()) + Convert.ToDecimal(pobj.Ds.Tables[0].Rows[0]["Shipamt"].ToString()) + Convert.ToDecimal(pobj.Ds.Tables[0].Rows[0]["Taxamt"].ToString()) - Convert.ToDecimal(pobj.Ds.Tables[0].Rows[0]["HappyMoney"].ToString()) - Convert.ToDecimal(pobj.Ds.Tables[0].Rows[0]["CreditGiven"].ToString())).ToString();
                txtSubTotal.Text = Total;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupOpenMPEMPEViewProduct", "PopupOpenMPEMPEViewProduct();", true);
                title.InnerText = "View Invoice - "+ Invoiceno;
            }
            
        }
        catch(Exception ex)
        { 
        
        }
    }
}