﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="InvoiceList.aspx.cs" Inherits="InvoiceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <style>
        .table-bordered th {
            vertical-align: middle;
        }

            .table-bordered th a {
                color: #ffffff;
            }

        #ContentPlaceHolder1_GridViewProduct tbody tr {
            white-space: nowrap;
            line-height: 1 !important;
        }

        .product {
            white-space: nowrap;
        }

        .pull-right {
            float: right;
        }

        .gridfloatright {
            text-align: right;
        }

        .form-control {
            border-radius: 5px !important;
            height: 35px !important;
        }

        .displayNone {
            display: none;
        }

        .productcode {
            white-space: nowrap;
        }

        .text-kty, .text-kty th a {
            color: #fff !important;
        }

        tr > th {
            background-color: #36b9cc !important;
        }

        .btn-save {
            text-transform: none !important;
            letter-spacing: 0 !important;
            min-width: 0px !important;
            padding: 0.375rem 0.75rem !important;
        }
        .label, label:not(.variant__button-label) {
            text-transform: none !important;
            letter-spacing: 0 !important;
            font-size: 1rem !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800 text-center" style="width: 100%">Your Orders
            <a href="Home.aspx" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm float-left btn-save">
                <i class="fas fa-arrow-left fa-sm text-white"></i><b style="padding-left: 10px;">Back</b>
            </a>
        </h1>

    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-2">
            <h6 class="m-0 font-weight-bold text-primary mt-1 mb-1">Your Orders</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:GridView ID="GridInvoice" runat="server" AutoGenerateColumns="False" AllowSorting="True" class="table table-bordered" OnSorting="GridInvoice_Sorting" OnRowDataBound="GridInvoice_RowDataBound">
                    <Columns>

                        <asp:TemplateField HeaderText="Invoice No" SortExpression="InvoiceNo" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lnkInvoiceNo" runat="server" Text='<%#Eval("InvoiceNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date" SortExpression="InvoiceDate" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="InvoiceStatus" runat="server" Text='<% #Eval("OrderStatusInfo")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bill To Address">
                            <ItemTemplate>
                                <asp:Label ID="BillingAddressNew" runat="server" Text='<% #Eval("BillingAddressNew")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ship To Address">
                            <ItemTemplate>
                                <asp:Label ID="ShippingAddress" runat="server" Text='<% #Eval("ShippingAddressNew")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="BillTotalamt" HeaderText="Amount" ItemStyle-HorizontalAlign="Right" />
                        <asp:TemplateField HeaderText="Tracking Info" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkTrackingNo" OnClick="btnTrackingNo_Click" runat="server" Text="Track Now" CommandArgument='<% #Eval("TrackingNo")%>' value='<% #Eval("TrackingNo")%>' Style="display: block;"></asp:LinkButton>
                                <asp:Label ID="DeliveredDate" runat="server" Text='<% #Eval("DeliveredDate")%>'></asp:Label>
                                <asp:Label ID="TrackingUrl" runat="server" Text='<% #Eval("TrackingUrl")%>' style="display:none"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnViewInvoice" runat="server" ForeColor="#157ab5" Text='View Invoice' OnClick="lbtnViewInvoice_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle CssClass="text-white bg-info text-center align-middle" />
                </asp:GridView>

            </div>
        </div>
    </div>

    <div id="MPEViewProduct" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" runat="server" id="title">View Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <asp:DataPagerGridView ID="GridViewProduct" runat="server" CssClass="table table-bordered"
                                    AutoGenerateColumns="False" Width="100%">
                                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                    <Columns>
                                        <asp:BoundField DataField="SkuID" HeaderText="Item" ReadOnly="True" />
                                        <asp:BoundField DataField="Productname" HeaderText="Description" ReadOnly="True" />
                                        <asp:TemplateField HeaderText="Qty" ItemStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblViewYourPrice" runat="server" Text='<%# Bind("TotalQty") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit price($)" ItemStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblViewPQty" runat="server" Text='<%# Bind("PUnitPrice") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="gridfloatright" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dis($)" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblViewDis" runat="server" Text='<%# Bind("Disc") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="gridfloatright" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total($)" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblViewTotal" runat="server" Text='<%# Bind("TotalAmt") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="gridfloatright" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="text-white bg-info text-center align-middle" />
                                </asp:DataPagerGridView>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row form-group">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-2">
                            <label>
                                INVOICE TOTAL
                            </label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtSubTotal" runat="server" CssClass="form-control" Enabled="false" Style="text-align: right"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-2">
                            <label>
                                PAYMENT TYPE
                            </label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtPaymentType" runat="server" CssClass="form-control" Enabled="false" Style="text-align: right"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function PopupOpenMPEMPEViewProduct() {
            $('#MPEViewProduct').modal('show');
        }
    </script>

</asp:Content>

