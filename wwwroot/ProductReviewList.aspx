﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ProductReviewList.aspx.cs" Inherits="ProductReviewList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table-bordered th {
            vertical-align: middle;
        }

            .table-bordered th a {
                color: #ffffff;
            }

        .text-kty, .text-kty th a {
            color: #fff !important;
        }

        tr > th {
            background-color: #36b9cc !important;
        }

        .btn-save {
            text-transform: none !important;
            letter-spacing: 0 !important;
            min-width: 0px !important;
            padding: 0.375rem 0.75rem !important;
        }

        .displayNone {
            display: none;
        }
    </style>
    <style type="text/css">
        .ratingStar {
            font-size: 0pt;
            width: 34px;
            height: 34px;
            margin: 0px;
            padding: 0px;
            cursor: pointer;
            display: block;
            background-repeat: no-repeat;
        }

        .filledStar {
            background-image: url(/image/FilledStar.png);
        }

        .emptyStar {
            background-image: url(/image/EmptyStar.png);
        }

        .savedStar {
            background-image: url(/image/SavedStar.png);
        }

        .pull-right {
            float: right;
        }

        .form-control {
            border-radius: 5px !important;
            height: 35px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800 text-center" style="width: 100%">Review your Products
            <a href="Home.aspx" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm float-left btn-save">
                <i class="fas fa-arrow-left fa-sm text-white"></i><b style="padding-left: 10px;">Back</b> </a>
        </h1>

    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-2">
            <h6 class="m-0 font-weight-bold text-primary mt-1 mb-1">Review your Products</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:GridView ID="GridProductLog" runat="server" AutoGenerateColumns="False" AllowSorting="True" class="table table-bordered" OnRowDataBound="GridProductLog_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ProductID" HeaderText="ProductID" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                        <asp:BoundField DataField="Rating" HeaderText="Rating" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone" />
                        <asp:BoundField DataField="ProductName" HeaderText="Product" ItemStyle-Width="40%" ItemStyle-CssClass="align--center" />
                        <asp:TemplateField HeaderText="Rating" ItemStyle-CssClass="align--center" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Rating ID="Rating1" runat="server" StarCssClass="ratingStar" WaitingStarCssClass="savedStar"
                                    FilledStarCssClass="filledStar" EmptyStarCssClass="emptyStar" CurrentRating="1" MaxRating="5">
                                </asp:Rating>
                            </ItemTemplate>
                            <HeaderStyle Wrap="False" BorderStyle="Outset" />
                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Comment" HeaderText="Review" ItemStyle-CssClass="align--center" ItemStyle-Width="30%"/>
                        <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="align--center" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnWriteReview" runat="server" ForeColor="#157ab5" Text='Write Review' OnClick="lbtnWriteReview_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="text-kty bg-info text-center align-middle" />
                </asp:GridView>

            </div>
        </div>
    </div>
    <div id="MPECancelOrder" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Write Your Review</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Rating
                                </label>

                            </div>
                        </div>
                        <div class="col-md-10">

                            <asp:Rating ID="RatingProduct" runat="server" StarCssClass="ratingStar" WaitingStarCssClass="savedStar"
                                FilledStarCssClass="filledStar" EmptyStarCssClass="emptyStar" CurrentRating="1" MaxRating="5" Style="margin-top: 6px;">
                            </asp:Rating>
                            <asp:HiddenField runat="server" ID="hdnProductId" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    Review
                                </label>
                                <asp:TextBox ID="txtReview" TextMode="MultiLine" Width="100%" runat="server" CssClass="form-control" Rows="2"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-success btn-sm pull-right btn-save"
                                Text="Save" OnClick="BtnSave_Click" />
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function PopupOpenMPECancelOrder() {
            $('#MPECancelOrder').modal('show');
        }
    </script>

</asp:Content>

